package com.brasil.naville.yougoclienteandroid.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.brasil.naville.yougoclienteandroid.Controller.Controller_Corridas_Realizadas;
import com.brasil.naville.yougoclienteandroid.R;

import java.util.ArrayList;

public class Adapter_Corridas_Realizadas extends BaseAdapter {

    boolean mEditing = false;

    RecyclerView.ViewHolder holder;

    private Context context;
    private final ArrayList<Controller_Corridas_Realizadas> arrayCorridasRealizadas;
    private int resource;
    private LayoutInflater layoutInflater;


    public Adapter_Corridas_Realizadas(Context context, int resource, ArrayList<Controller_Corridas_Realizadas> arrayCorridasRealizadas) {
        this.context = context;
        this.arrayCorridasRealizadas = arrayCorridasRealizadas;
        this.resource = resource;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayCorridasRealizadas.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayCorridasRealizadas.get(position);
    }

    @Override
    public long getItemId(int position) {
        Controller_Corridas_Realizadas controllerProdutos = arrayCorridasRealizadas.get(position);
        return Long.parseLong(controllerProdutos.get(Controller_Corridas_Realizadas.id_corrida));
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView == null) {
            convertView = layoutInflater.inflate(resource, parent, false);

        }

        final Controller_Corridas_Realizadas controllerCarrinho = arrayCorridasRealizadas.get(position);

        TextView txtDataInicioCorridasRealizadas = convertView.findViewById(R.id.txtDataInicioCorridasRealizadas);
        TextView txtValorCorridasRealizadas = convertView.findViewById(R.id.txtValorCorridasRealizadas);
        TextView txtEnderecoOrigemCorridasRealizadas = convertView.findViewById(R.id.txtEnderecoOrigemCorridasRealizadas);
        TextView txtEnderecoDestinoCorridasRealizadas = convertView.findViewById(R.id.txtEnderecoDestinoCorridasRealizadas);
        TextView txtNomeUsuarioCorridasRealizadas = convertView.findViewById(R.id.txtNomeUsuarioCorridasRealizadas);

        txtDataInicioCorridasRealizadas.setText(controllerCarrinho.get(Controller_Corridas_Realizadas.data_corrida_inicio));
        txtValorCorridasRealizadas.setText(controllerCarrinho.get(Controller_Corridas_Realizadas.valor_corrida));
        txtEnderecoOrigemCorridasRealizadas.setText(controllerCarrinho.get(Controller_Corridas_Realizadas.endereco_origem));
        txtEnderecoDestinoCorridasRealizadas.setText(controllerCarrinho.get(Controller_Corridas_Realizadas.endereco_destino));
        txtNomeUsuarioCorridasRealizadas.setText(controllerCarrinho.get(Controller_Corridas_Realizadas.nome_usuario));


        mEditing = false;


        return convertView;
    }
}
