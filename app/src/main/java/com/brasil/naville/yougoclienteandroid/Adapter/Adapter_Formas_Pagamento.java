package com.brasil.naville.yougoclienteandroid.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.brasil.naville.yougoclienteandroid.Controller.Controller_Formas_Pagamento;
import com.brasil.naville.yougoclienteandroid.R;

import java.util.ArrayList;

import static android.support.v7.widget.RecyclerView.*;

public class Adapter_Formas_Pagamento extends BaseAdapter {


    boolean mEditing = false;

    ViewHolder holder;

    private Context context;
    private final ArrayList<Controller_Formas_Pagamento> arrayFormaDePagamento;
    private int resource;
    private LayoutInflater layoutInflater;


    public Adapter_Formas_Pagamento(Context context, int resource, ArrayList<Controller_Formas_Pagamento> arrayFormaDePagamento) {
        this.context = context;
        this.arrayFormaDePagamento = arrayFormaDePagamento;
        this.resource = resource;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayFormaDePagamento.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayFormaDePagamento.get(position);
    }

    @Override
    public long getItemId(int position) {
        Controller_Formas_Pagamento controllerProdutos = arrayFormaDePagamento.get(position);
        return Long.parseLong(controllerProdutos.get(Controller_Formas_Pagamento.id_pagamento));
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (convertView == null) {
            convertView = layoutInflater.inflate(resource, parent, false);

        }

        final Controller_Formas_Pagamento controllerCarrinho = arrayFormaDePagamento.get(position);

        TextView txtVisualizarDataCartao = convertView.findViewById(R.id.txtVisualizarDataCartao);
        TextView txtVisualizarNomeCartao = convertView.findViewById(R.id.txtVisualizarNomeCartao);
        TextView txtVisualizarNumeroCartao = convertView.findViewById(R.id.txtVisualizarNumeroCartao);

        txtVisualizarDataCartao.setText(controllerCarrinho.get(Controller_Formas_Pagamento.data_vencimento_cartao));
        txtVisualizarNumeroCartao.setText(controllerCarrinho.get(Controller_Formas_Pagamento.numero_cartao));
        txtVisualizarNomeCartao.setText(controllerCarrinho.get(Controller_Formas_Pagamento.nome_cartao));


        mEditing = false;


        return convertView;
    }
}
