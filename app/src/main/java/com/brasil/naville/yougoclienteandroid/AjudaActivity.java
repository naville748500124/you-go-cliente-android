package com.brasil.naville.yougoclienteandroid;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import static android.R.id.message;

public class AjudaActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgVoltarAjuda;
    Button btnAjudaLigar;
    Button btnAjudaEmail;
    AppShared sessao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajuda);

        imgVoltarAjuda = findViewById(R.id.imgVoltarAjuda);
        btnAjudaLigar = findViewById(R.id.btnAjudaLigar);
        btnAjudaEmail = findViewById(R.id.btnAjudaEmail);
        sessao = new AppShared(this);

        btnAjudaLigar.setOnClickListener(this);
        btnAjudaEmail.setOnClickListener(this);


        imgVoltarAjuda.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == imgVoltarAjuda) {
            finish();
        } else if (view == btnAjudaLigar) {

            Intent i = new Intent(Intent.ACTION_DIAL);
            String p = "tel:" + sessao.getTelefoneAjuda();
            i.setData(Uri.parse(p));
            startActivity(i);

        } else if (view == btnAjudaEmail) {
            Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", sessao.getEmailAjuda(), null));
            String subject = "Contato YouGo";
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_TEXT, message);
            startActivity(Intent.createChooser(intent, "Choose an Email client :"));

        }

    }
}
