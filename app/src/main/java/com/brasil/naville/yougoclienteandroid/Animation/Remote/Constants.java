package com.brasil.naville.yougoclienteandroid.Animation.Remote;


public class Constants {

    public static final String baseURL = "https://googleapis.com";

    public static IGoogleAPI getGoogleApi() {
        return RetrofitClient.getClient(baseURL).create(IGoogleAPI.class);
    }
}
