package com.brasil.naville.yougoclienteandroid;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

public class AppShared {
    public static final String PREFERENCE_NAME = "INFO";
    private final SharedPreferences sharedpreferences;
    public static Integer tempo = 3;


    public AppShared(Context context) {
        sharedpreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public String getCellphone() {
        String cellphone = sharedpreferences.getString("cellphone", "");
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("cellphone", cellphone);
        editor.commit();
    }

    public String getTelefoneAjuda() {
        String telefoneAjuda = sharedpreferences.getString("telefoneAjuda", "");
        return telefoneAjuda;
    }

    public void setTelefoneAjuda(String telefoneAjuda) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("telefoneAjuda", telefoneAjuda);
        editor.commit();
    }

    public String getEmailAjuda() {
        String emailAjuda = sharedpreferences.getString("emailAjuda", "");
        return emailAjuda;
    }

    public void setEmailAjuda(String emailAjuda) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("emailAjuda", emailAjuda);
        editor.commit();
    }

    public String getIdUsuario() {
        String idUsuario = sharedpreferences.getString("idUsuario", "");
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("idUsuario", idUsuario);
        editor.commit();
    }


    public String getNomePassageiro() {
        String nomePassageiro = sharedpreferences.getString("nomePassageiro", "");
        return nomePassageiro;
    }

    public void setNomePassageiro(String nomePassageiro) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("nomePassageiro", nomePassageiro);
        editor.commit();
    }


    public String getStatusCorridaRealizada() {
        String statusCorridaRealizada = sharedpreferences.getString("statusCorridaRealizada", "");
        return statusCorridaRealizada;
    }

    public void setStatusCorridaRealizada(String statusCorridaRealizada) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("statusCorridaRealizada", statusCorridaRealizada);
        editor.commit();
    }


    public String getMotivoCancelamentoCorridaRealizada() {
        String motivoCancelamentoCorridaRealizada = sharedpreferences.getString("motivoCancelamentoCorridaRealizada", "");
        return motivoCancelamentoCorridaRealizada;
    }

    public void setMotivoCancelamentoCorridaRealizada(String motivoCancelamentoCorridaRealizada) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("motivoCancelamentoCorridaRealizada", motivoCancelamentoCorridaRealizada);
        editor.commit();
    }


    public String getPlacaCarroCorridaRealizada() {
        String placaCarroCorrida = sharedpreferences.getString("placaCarroCorrida", "");
        return placaCarroCorrida;
    }

    public void setPlacaCarroCorridaRealizada(String placaCarroCorrida) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("placaCarroCorrida", placaCarroCorrida);
        editor.commit();
    }


    public String getCarroCorridaRealizada() {
        String carroCorridaRealizada = sharedpreferences.getString("carroCorridaRealizada", "");
        return carroCorridaRealizada;
    }

    public void setCarroCorridaRealizada(String carroCorridaRealizada) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("carroCorridaRealizada", carroCorridaRealizada);
        editor.commit();
    }


    public String getKmCorridaRealizada() {
        String kmCorridaRealizada = sharedpreferences.getString("kmCorridaRealizada", "");
        return kmCorridaRealizada;
    }

    public void setKmCorridaRealizada(String kmCorridaRealizada) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("kmCorridaRealizada", kmCorridaRealizada);
        editor.commit();
    }


    public String getDataFimCorridaRealizada() {
        String dataFimCorridaRealizada = sharedpreferences.getString("dataFimCorridaRealizada", "");
        return dataFimCorridaRealizada;
    }

    public void setDataFimCorridaRealizada(String dataFimCorridaRealizada) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("dataFimCorridaRealizada", dataFimCorridaRealizada);
        editor.commit();
    }


    public String getIdUsuarioCorridaRealizada() {
        String idUsuarioCorridaRealizada = sharedpreferences.getString("idUsuarioCorridaRealizada", "");
        return idUsuarioCorridaRealizada;
    }

    public void setIdUsuarioCorridaRealizada(String idUsuarioCorridaRealizada) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("idUsuarioCorridaRealizada", idUsuarioCorridaRealizada);
        editor.commit();
    }


    public String getNomeUsuarioCorridaRealizada() {
        String nomeUsuarioCorridaRealizada = sharedpreferences.getString("nomeUsuarioCorridaRealizada", "");
        return nomeUsuarioCorridaRealizada;
    }

    public void setNomeUsuarioCorridaRealizada(String nomeUsuarioCorridaRealizada) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("nomeUsuarioCorridaRealizada", nomeUsuarioCorridaRealizada);
        editor.commit();
    }


    public String getEnderecoDestinoCorridaRealizada() {
        String enderecoDestinoCorridaRealizada = sharedpreferences.getString("enderecoDestinoCorridaRealizada", "");
        return enderecoDestinoCorridaRealizada;
    }

    public void setEnderecoDestinoCorridaRealizada(String enderecoDestinoCorridaRealizada) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("enderecoDestinoCorridaRealizada", enderecoDestinoCorridaRealizada);
        editor.commit();
    }


    public String getEnderecoOrigemCorridaRealizada() {
        String enderecoOrigemCorridaRealizada = sharedpreferences.getString("enderecoOrigemCorridaRealizada", "");
        return enderecoOrigemCorridaRealizada;
    }

    public void setEnderecoOrigemCorridaRealizada(String enderecoOrigemCorridaRealizada) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("enderecoOrigemCorridaRealizada", enderecoOrigemCorridaRealizada);
        editor.commit();
    }


    public String getValorCorridaRealizada() {
        String valorCorridaRealizada = sharedpreferences.getString("valorCorridaRealizada", "");
        return valorCorridaRealizada;
    }

    public void setValorCorridaRealizada(String valorCorridaRealizada) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("valorCorridaRealizada", valorCorridaRealizada);
        editor.commit();
    }


    public String getDataCorridaRealizada() {
        String dataCorridaRealizada = sharedpreferences.getString("dataCorridaRealizada", "");
        return dataCorridaRealizada;
    }

    public void setDataCorridaRealizada(String dataCorridaRealizada) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("dataCorridaRealizada", dataCorridaRealizada);
        editor.commit();
    }


    public String getIDCorridaRealizada() {
        String idCorridaRealizada = sharedpreferences.getString("idCorridaRealizada", "");
        return idCorridaRealizada;
    }

    public void setIDCorridaRealizada(String idCorridaRealizada) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("idCorridaRealizada", idCorridaRealizada);
        editor.commit();
    }


    public String getIdCorrida() {
        String idCorrida = sharedpreferences.getString("idCorrida", "");
        return idCorrida;
    }

    public void setIdCorrida(String idCorrida) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("idCorrida", idCorrida);
        editor.commit();
    }


    public String getLatitudeOrigem() {
        String latitudeOrigem = sharedpreferences.getString("latitudeOrigem", "");
        return latitudeOrigem;
    }

    public void setLatitudeOrigem(String latitudeOrigem) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("latitudeOrigem", latitudeOrigem);
        editor.commit();
    }


    public String getLongitudeOrigem() {
        String longitudeOrigem = sharedpreferences.getString("longitudeOrigem", "");
        return longitudeOrigem;
    }

    public void setLongitudeOrigem(String longitudeOrigem) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("longitudeOrigem", longitudeOrigem);
        editor.commit();
    }


    public String getlatitudeDestino() {
        String latitudeDestino = sharedpreferences.getString("latitudeDestino", "");
        return latitudeDestino;
    }

    public void setLatitudeDestino(String latitudeDestino) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("latitudeDestino", latitudeDestino);
        editor.commit();
    }


    public String getLongitudeDestino() {
        String longitudeDestino = sharedpreferences.getString("longitudeDestino", "");
        return longitudeDestino;
    }

    public void setLongitudeDestino(String longitudeDestino) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("longitudeDestino", longitudeDestino);
        editor.commit();
    }


    public String getIdMarca() {
        String idMarcaShared = sharedpreferences.getString("idMarcaShared", "");
        return idMarcaShared;
    }

    public void setIdMarca(String idMarcaShared) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("idMarcaShared", idMarcaShared);
        editor.commit();
    }


    public String getEnderecoOrigem() {
        String enderecoOrigem = sharedpreferences.getString("enderecoOrigem", "");
        return enderecoOrigem;
    }

    public void setEnderecoOrigem(String enderecoOrigem) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("enderecoOrigem", enderecoOrigem);
        editor.commit();
    }


    public String getEnderecoDestino() {
        String enderecoDestino = sharedpreferences.getString("enderecoDestino", "");
        return enderecoDestino;
    }

    public void setEnderecoDestino(String enderecoDestino) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("enderecoDestino", enderecoDestino);
        editor.commit();
    }

    public String getDataCartao() {
        String dataCartao = sharedpreferences.getString("dataCartao", "");
        return dataCartao;
    }

    public void setDataCartao(String dataCartao) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("dataCartao", dataCartao);
        editor.commit();
    }

    public String getNumeroCartao() {
        String numeroCartao = sharedpreferences.getString("numeroCartao", "");
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("numeroCartao", numeroCartao);
        editor.commit();
    }

    public String getNomeCartao() {
        String nomeCartao = sharedpreferences.getString("nomeCartao", "");
        return nomeCartao;
    }

    public void setNomeCartao(String nomeCartao) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("nomeCartao", nomeCartao);
        editor.commit();
    }


    public String getIDPagamento() {
        String idPagamento = sharedpreferences.getString("idPagamento", "");
        return idPagamento;
    }

    public void setIDPagamento(String idPagamento) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("idPagamento", idPagamento);
        editor.commit();
    }


    public String getIdBanco() {
        String idBancoShared = sharedpreferences.getString("idBancoShared", "");
        return idBancoShared;
    }

    public void setIdBanco(String idBancoShared) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("idBancoShared", idBancoShared);
        editor.commit();
    }

    public String getEmailMotorista() {
        String emailMotorista = sharedpreferences.getString("emailMotorista", "");
        return emailMotorista;
    }

    public void setEmailMotorista(String emailMotorista) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("emailMotorista", emailMotorista);
        editor.commit();
    }

    public String getSenhaMotorista() {
        String senhaMotorista = sharedpreferences.getString("senhaMotorista", "");
        return senhaMotorista;
    }

    public void setSenhaMotorista(String senhaMotorista) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("senhaMotorista", senhaMotorista);
        editor.commit();
    }

    public String getPass() {
        String senha = sharedpreferences.getString("pass", "");
        return senha;
    }

    public void setPass(String pass) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("pass", pass);
        editor.commit();
    }

    public void setPin(String pin) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("pin", pin);
        editor.commit();
    }

    public String getPin() {
        String pin = sharedpreferences.getString("pin", "");
        return pin;
    }

    public void setLatitude(String latitude) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("latitude", latitude);
        editor.commit();
    }

    public String getLatitude() {
        String latitude = sharedpreferences.getString("latitude", "");
        return latitude;
    }

    public void setLongitude(String longitude) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("longitude", longitude);
        editor.commit();
    }

    public String getLongitude() {
        String longitude = sharedpreferences.getString("longitude", "");
        return longitude;
    }

    public void setSwitchTeste(int switchTeste) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("switchTeste", switchTeste);
        editor.commit();
    }

    public int getSwitchTeste() {
        int switchTeste = sharedpreferences.getInt("switchTeste", 0);
        return switchTeste;
    }

    public void setSpinnerGroupID(String spinnerID) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("spinnerID", spinnerID);
        editor.commit();
    }

    public String getSpinnerGroupID() {
        String spinnerID = sharedpreferences.getString("spinnerID", "");
        return spinnerID;
    }

    public void setMacAddress(String macAddress) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("macAddress", macAddress);
        editor.commit();
    }

    public String getMacAddress() {
        String macAddress = sharedpreferences.getString("macAddress", "");
        return macAddress;
    }

    public void clear() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove("cellphone");
        editor.remove("pass");
        editor.clear();
        editor.commit();
    }

    public void setIDFacebook(String idFacebook) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("idFacebook", idFacebook);
        editor.commit();
    }

    public String getIDFacebook() {
        String idFacebook = sharedpreferences.getString("idFacebook", "");
        return idFacebook;
    }

    public static class VerifyNetWork extends AsyncTask<String, String, String> {

        private String resp;


        @Override
        protected String doInBackground(String... strings) {
            System.out.println("THREAD RODANDO...");

            try {
                tempo = Integer.parseInt(strings[0]) * 1000;
                Thread.sleep(tempo);
                System.out.println("SLEEP POR = " + strings[0] + " SEGUNDOS!");


            } catch (Exception e) {
                e.printStackTrace();
                resp = e.getMessage();
            }

            return resp;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("RESULTADO DA THREAD = " + s);

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            System.out.println("ESPERAR POR " + tempo + " SEGUNDOS!");

        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            System.out.println(values[0]);

        }
    }

    public void setGuardaPin2(int guardaPin2) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("guardaPin2", guardaPin2);
        editor.commit();
    }

    public int getIDGrupo() {
        int idDoGrupo = sharedpreferences.getInt("idDoGrupo", 0);
        return idDoGrupo;
    }

    public void setIdDoGrupo(int idDoGrupo) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("idDoGrupo", idDoGrupo);
        editor.commit();
    }

    public int getGuardaPin2() {
        int switchTeste = sharedpreferences.getInt("guardaPin2", 0);
        return switchTeste;
    }


    public void setTokenAcessoLogin(String tokenAcessoLogin){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("tokenAcessoLogin", tokenAcessoLogin);
        editor.commit();
    }


    public String getTokenAcessoLogin(){
        String tokenAcessoLogin = sharedpreferences.getString("tokenAcessoLogin", "");
        return tokenAcessoLogin;
    }

    public void setCidadeOrigem(String cidadeOrigem){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("cidadeOrigem", cidadeOrigem);
        editor.commit();
    }


    public String getCidadeOrigem(){
        String cidadeOrigem = sharedpreferences.getString("cidadeOrigem", "");
        return cidadeOrigem;
    }

    public void setUltimaCorrida(String ultimaCorrida){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("ultimaCorrida", ultimaCorrida);
        editor.commit();
    }


    public String getUltimaCorrida(){
        String ultimaCorrida = sharedpreferences.getString("ultimaCorrida", "");
        return ultimaCorrida;
    }


}