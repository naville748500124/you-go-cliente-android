package com.brasil.naville.yougoclienteandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.brasil.naville.yougoclienteandroid.WS.WebService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AvaliacoesRecebidasActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgVoltarAvaliacoesRecebidas;
    RatingBar rating_rating_barPontualidade;
    RatingBar rating_rating_barSimpatia;
    AppShared sessao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avaliacoes_recebidas);

        imgVoltarAvaliacoesRecebidas = findViewById(R.id.imgVoltarAvaliacoesRecebidas);
        rating_rating_barPontualidade = findViewById(R.id.rating_rating_barPontualidade);
        rating_rating_barSimpatia = findViewById(R.id.rating_rating_barSimpatia);


        sessao = new AppShared(this);


        imgVoltarAvaliacoesRecebidas.setOnClickListener(this);

        media_avaliacoes();


    }

    @Override
    public void onClick(View view) {
        if (view == imgVoltarAvaliacoesRecebidas) {
            finish();
        }

    }

    public void media_avaliacoes() {


        Map<String, String> parametros = new HashMap<>();
        parametros.put("id_corrida", sessao.getIdCorrida());


        System.out.println(parametros);

        WebService ws = new WebService(
                getString(R.string.servidor),
                "media_avaliacoes",
                "GET",
                null,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");
                    String avaliacoes = objeto.getString("avaliacoes");


                    System.out.println("AS AVALIAÇÕES SÃO: " + avaliacoes);


                    if (avaliacoes.equals("null")) {
                        Toast.makeText(AvaliacoesRecebidasActivity.this, "Você ainda não tem avaliações, por enquanto sua média será 3 como padrão.", Toast.LENGTH_SHORT).show();

                    }


                    JSONObject arrayInterno = objeto.getJSONObject("avaliacoes");

                    String pontualidade_passageiro = arrayInterno.getString("pontualidade_passageiro");
                    String simpatia_passageiro = arrayInterno.getString("simpatia_passageiro");


                    System.out.println("STATUS DAS AVALIAÇOES MEDIAS DO PASSAGEIRO É = " + status);
                    System.out.println("RESULTADO DAS AVALIACOES MEDIAS DO PASSAGEIRO É = " + resultado);
                    System.out.println("MEDIA DA PONTUALIDADE DO PASSAGEIRO É = " + pontualidade_passageiro);
                    System.out.println("MEDIA DA SIMPATIA DO PASSAGEIRO É = " + simpatia_passageiro);


                    rating_rating_barPontualidade.setRating(Float.parseFloat(pontualidade_passageiro));
                    rating_rating_barSimpatia.setRating(Float.parseFloat(simpatia_passageiro));


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }
}
