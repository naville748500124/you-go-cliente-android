package com.brasil.naville.yougoclienteandroid;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brasil.naville.yougoclienteandroid.Util.Mask;
import com.brasil.naville.yougoclienteandroid.WS.WebService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class CadastrarPagamentosActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgCadastrarPagamento;
    EditText edtCadastrarNumeroCartao;
    EditText edtCadastrarNomeCartao;
    EditText edtCadastrarDataCartao;
    EditText edtCadastrarCVVCartao;
    Button btnCadastrarCartao;
    LinearLayout lnlCadastrarPagamentos;
    AppShared sessao;
    TextView pickerDataNascimentoCadastroCartao;
    public int dia, mes, ano;
    DatePicker date;
    String data;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_pagamentos);

        imgCadastrarPagamento = findViewById(R.id.imgCadastrarPagamento);
        edtCadastrarNumeroCartao = findViewById(R.id.edtCadastrarNumeroCartao);
        edtCadastrarNomeCartao = findViewById(R.id.edtCadastrarNomeCartao);
        edtCadastrarDataCartao = findViewById(R.id.edtCadastrarDataCartao);
        edtCadastrarCVVCartao = findViewById(R.id.edtCadastrarCVVCartao);
        btnCadastrarCartao = findViewById(R.id.btnCadastrarCartao);
        lnlCadastrarPagamentos = findViewById(R.id.lnlCadastrarPagamentos);
        date = findViewById(R.id.date);
        pickerDataNascimentoCadastroCartao = findViewById(R.id.pickerDataNascimentoCadastroCartao);

        sessao = new AppShared(this);


        imgCadastrarPagamento.setOnClickListener(this);
        edtCadastrarNumeroCartao.setOnClickListener(this);
        edtCadastrarNomeCartao.setOnClickListener(this);
        edtCadastrarDataCartao.setOnClickListener(this);
        edtCadastrarCVVCartao.setOnClickListener(this);
        btnCadastrarCartao.setOnClickListener(this);
        lnlCadastrarPagamentos.setOnClickListener(this);

        TextWatcher dataMask = Mask.insert("####/##", edtCadastrarDataCartao);
        edtCadastrarDataCartao.addTextChangedListener(dataMask);

        TextWatcher numeroCartaoMask = Mask.insert("####.####.####.####", edtCadastrarNumeroCartao);
        edtCadastrarNumeroCartao.addTextChangedListener(numeroCartaoMask);


        //Tirar o teclado quando clicar fora
        lnlCadastrarPagamentos.setOnTouchListener(new View.OnTouchListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
                return false;
            }
        });

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        //PICKER DATA
        pickerDataNascimentoCadastroCartao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                atualizarData();
            }
        });

    }


    public void atualizarData() {

        final DatePickerDialog datePickerDialog = new DatePickerDialog(CadastrarPagamentosActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                monthOfYear = monthOfYear + 1;
                final String dataAtual = dayOfMonth + "/" + monthOfYear + "/" + year;
                pickerDataNascimentoCadastroCartao.setText(dayOfMonth + "/" + monthOfYear + "/" + year);
                data = year + "-" + monthOfYear + "-" + dayOfMonth;

                System.out.println("DATA ATUAL = " + dataAtual);


            }
        }, ano, mes - 1, dia);
        datePickerDialog.getDatePicker();
        datePickerDialog.show();
    }

    @Override
    public void onClick(View v) {
        if (v == imgCadastrarPagamento) {
            finish();
        } else if (v == btnCadastrarCartao) {

            if (edtCadastrarNumeroCartao.getText().toString().equals("")) {
                Toast.makeText(this, "O campo número do cartão está vazio.", Toast.LENGTH_SHORT).show();
            } else if (edtCadastrarNomeCartao.getText().toString().equals("")) {
                Toast.makeText(this, "O campo nome está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtCadastrarDataCartao.getText().toString().equals("")) {
                Toast.makeText(this, "O campo data de epiração está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtCadastrarCVVCartao.getText().toString().equals("")) {
                Toast.makeText(this, "O campo CVV está vazio.", Toast.LENGTH_SHORT).show();

            } else {
                cadastrarCartao();
            }

        }

    }

    public void cadastrarCartao() {


        Map<String, String> parametros = new HashMap<>();
        parametros.put("nome_cartao", edtCadastrarNomeCartao.getText().toString());
        parametros.put("numero_cartao", edtCadastrarNumeroCartao.getText().toString());
        parametros.put("data_vencimento_cartao", edtCadastrarDataCartao.getText().toString().replace("/", "") + "01");
        parametros.put("cvv_cartao", edtCadastrarCVVCartao.getText().toString());
        parametros.put("data_nascimento", data.replace("/", ""));

        WebService ws = new WebService(
                getString(R.string.servidor),
                "nova_forma_pagamento",
                "POST",
                parametros,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");


                    System.out.println("STATUS DO CADASTRO DA FORMA DO PAGAMENTO É = " + status);
                    System.out.println("RESULTADO DO CADASTRO DA FORMA DO PAGAMENTO É = " + resultado);


                    if (status == 1) {
                        Intent intent = new Intent(CadastrarPagamentosActivity.this, PagamentosActivity.class);
                        startActivity(intent);
                        finish();
                        Toast.makeText(CadastrarPagamentosActivity.this, "Sua forma de pagamento foi cadastrada com sucesso.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CadastrarPagamentosActivity.this, "Erro ao cadastrar sua forma de pagamento, tente novamente.", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }


}
