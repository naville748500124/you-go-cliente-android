package com.brasil.naville.yougoclienteandroid;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brasil.naville.yougoclienteandroid.Util.Mask;
import com.brasil.naville.yougoclienteandroid.Util.StringEncryption;
import com.brasil.naville.yougoclienteandroid.WS.WebService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class CadastroActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgVoltarCadastro;

    LinearLayout lnlCadastro1;
    ScrollView scrCadastro1;
    Button btnCadastroAvancar1;
    String stringSHA1;

    EditText edtCadastroNome;
    Spinner spnCadastroSexo;
    EditText edtCadastroCadastroRG;
    EditText edtCadastroCPF;
    EditText edtCadastroEmail;
    EditText edtCadastroConfirmarEmail;
    EditText edtCadastroSenha;
    EditText edtCadastroConfirmarSenha;
    EditText edtCadastroTelefone;
    EditText edtCadastroCelular;
    EditText edtCadastroCEP;
    EditText edtCadastroLogradoutro;
    EditText edtCadastroBairro;
    EditText edtCadastroCidade;
    EditText edtCadastroComplemento;
    Spinner spnCadastroUF;
    TextView txtTermosDeUso;
    String email;
    String emailPattern;
    String email2;
    String emailPattern2;
    String[] camposEstados = {"AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"};
    String[] camposSexo = {"Feminino", "Masculino"};
    Button btnCadastroFotoPessoal;
    LinearLayout lnlFoto1;
    ImageView imgFotoPessoal;
    String perfilBase64;
    String imagemBase64;
    private boolean buscar_cep = false;
    EditText edtCadastroNumero;
    int idUF;
    int idGenero;
    String base64Final;

    Dialog alertDialog_show;

    public static int RESULT_GALLERY = 0;

    private int RESULT_LOAD_IMG = 1;
    private int REQUEST_IMAGE_CAPTURE = 2;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        imgVoltarCadastro = findViewById(R.id.imgVoltarCadastro);
        btnCadastroAvancar1 = findViewById(R.id.btnCadastroSalvar);
        lnlCadastro1 = findViewById(R.id.lnlCadastro);
        scrCadastro1 = findViewById(R.id.scrCadastro1);
        btnCadastroFotoPessoal = findViewById(R.id.btnCadastroFotoPessoal);
        lnlFoto1 = findViewById(R.id.lnlFoto1);
        imgFotoPessoal = findViewById(R.id.imgFotoPessoal);
        edtCadastroNumero = findViewById(R.id.edtCadastroNumero);
        txtTermosDeUso = findViewById(R.id.txtTermosDeUso);


        ViewGroup.LayoutParams params = lnlFoto1.getLayoutParams();
        params.height = 0;
        lnlFoto1.setLayoutParams(params);


        btnCadastroFotoPessoal.setOnClickListener(this);
        lnlFoto1.setOnClickListener(this);
        imgFotoPessoal.setOnClickListener(this);
        edtCadastroNumero.setOnClickListener(this);

        lnlCadastro1.setOnClickListener(this);
        scrCadastro1.setOnClickListener(this);
        btnCadastroAvancar1.setOnClickListener(this);
        edtCadastroNome = findViewById(R.id.edtCadastroNome);
        spnCadastroSexo = findViewById(R.id.spnCadastroSexo);
        edtCadastroCadastroRG = findViewById(R.id.edtCadastroRG);
        edtCadastroCPF = findViewById(R.id.edtCadastroCPF);
        edtCadastroEmail = findViewById(R.id.edtCadastroEmail);
        edtCadastroConfirmarEmail = findViewById(R.id.edtCadastroConfirmarEmail);
        edtCadastroSenha = findViewById(R.id.edtCadastroSenha);
        edtCadastroConfirmarSenha = findViewById(R.id.edtCadastroConfirmarSenha);
        edtCadastroTelefone = findViewById(R.id.edtCadastroTelefone);
        edtCadastroCelular = findViewById(R.id.edtCadastroCelular);
        edtCadastroCPF = findViewById(R.id.edtCadastroCPF);
        edtCadastroCEP = findViewById(R.id.edtCadastroCEP);
        edtCadastroLogradoutro = findViewById(R.id.edtCadastroLogradoutro);
        edtCadastroBairro = findViewById(R.id.edtCadastroBairro);
        edtCadastroCidade = findViewById(R.id.edtCadastroCidade);
        spnCadastroUF = findViewById(R.id.spnCadastroUF);
        edtCadastroComplemento = findViewById(R.id.edtCadastroComplemento);

        txtTermosDeUso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CadastroActivity.this, TermosActivity.class);
                startActivity(intent);
            }
        });

        imgVoltarCadastro.setOnClickListener(this);

        TextWatcher telefoneMask = Mask.insert("(##)####-####", edtCadastroTelefone);
        edtCadastroTelefone.addTextChangedListener(telefoneMask);

        TextWatcher celularMask = Mask.insert("(##)#####-####", edtCadastroCelular);
        edtCadastroCelular.addTextChangedListener(celularMask);

        TextWatcher cpfMask = Mask.insert("###.###.###-##", edtCadastroCPF);
        edtCadastroCPF.addTextChangedListener(cpfMask);


        TextWatcher cepMask = Mask.insert("#####-###", edtCadastroCEP);
        edtCadastroCEP.addTextChangedListener(cepMask);


        TextWatcher RGWatcher = Mask.insert("##.###.###-#", edtCadastroCadastroRG, 10, InputType.TYPE_CLASS_NUMBER, InputType.TYPE_CLASS_TEXT);
        edtCadastroCadastroRG.addTextChangedListener(RGWatcher);


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, camposEstados);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnCadastroUF.setAdapter(spinnerArrayAdapter);

        ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, camposSexo);
        spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnCadastroSexo.setAdapter(spinnerArrayAdapter2);


//        edtCadastroCEP.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    viaCEP();
//                }
//            }
//        });


        edtCadastroCEP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (count == 9) {
                    if (buscar_cep) {
                        viaCEP();
                    }

                } else {
                    buscar_cep = true;
                }

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        //Tirar o teclado quando clicar fora
        lnlCadastro1.setOnTouchListener(new View.OnTouchListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        scrCadastro1.setOnTouchListener(new View.OnTouchListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
                return false;
            }
        });


        for (int i = 1; i < camposSexo.length; i++) {
            spnCadastroSexo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        idGenero = 81;
                        System.out.println("O ID FO GENERO SELECIONADO É = " + idGenero);
                    } else {
                        idGenero = 82;
                        System.out.println("O ID DO GENERO SELECIONADO É = " + idGenero);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


    }


    boolean blFoto1 = false;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View view) {

        if (view == imgVoltarCadastro) {
            finish();
        } else if (view == btnCadastroFotoPessoal) {


            if (!blFoto1) {

                ViewGroup.LayoutParams params = lnlFoto1.getLayoutParams();


                params.height = params.height + 300;

                lnlFoto1.setLayoutParams(params);
                blFoto1 = true;

            } else {
                ViewGroup.LayoutParams params = lnlFoto1.getLayoutParams();
                params.height = params.height - 300;
                lnlFoto1.setLayoutParams(params);
                blFoto1 = false;

            }
        } else if (view == imgFotoPessoal) {
            chamaImagem();


        } else if (view == btnCadastroAvancar1) {

            email = edtCadastroEmail.getText().toString().trim();
            emailPattern = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";


            email2 = edtCadastroConfirmarEmail.getText().toString().trim();
            emailPattern2 = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

            if (edtCadastroNome.getText().toString().equals("")) {
                Toast.makeText(this, "O campo nome está vazio.", Toast.LENGTH_SHORT).show();
            } else if (edtCadastroCadastroRG.getText().toString().equals("")) {
                Toast.makeText(this, "O campo RG está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtCadastroCPF.getText().toString().equals("")) {
                Toast.makeText(this, "O campo CPF está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtCadastroEmail.getText().toString().equals("")) {
                Toast.makeText(this, "O campo e-mail está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtCadastroConfirmarEmail.getText().toString().equals("")) {
                Toast.makeText(this, "O campo confirmar e-mail está vazio.", Toast.LENGTH_SHORT).show();

            } else if (!email.matches(emailPattern)) {

                Toast.makeText(getApplicationContext(), "Digite um e-mail válido.", Toast.LENGTH_SHORT).show();


            } else if (!Objects.equals(edtCadastroConfirmarEmail.getText().toString(), edtCadastroEmail.getText().toString())) {

                Toast.makeText(getApplicationContext(), "O e-mail e a confirmação tem que ser iguais.", Toast.LENGTH_SHORT).show();
            } else if (edtCadastroSenha.getText().toString().equals("")) {
                Toast.makeText(this, "O campo senha está vazio.", Toast.LENGTH_SHORT).show();


            } else if (edtCadastroSenha.length() <= 7) {
                Toast.makeText(this, "Sua senha precisa ter no mínimo 8 caracteres.", Toast.LENGTH_SHORT).show();
            } else if (!Objects.equals(edtCadastroConfirmarSenha.getText().toString(), edtCadastroSenha.getText().toString())) {
                Toast.makeText(getApplicationContext(), "A senha e a confirmação tem que ser iguais.", Toast.LENGTH_SHORT).show();
            } else if (edtCadastroConfirmarSenha.getText().toString().equals("")) {
                Toast.makeText(this, "O campo confirmar senha está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtCadastroTelefone.getText().toString().equals("")) {
                Toast.makeText(this, "O campo telefone está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtCadastroCelular.getText().toString().equals("")) {
                Toast.makeText(this, "O campo celular está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtCadastroCEP.getText().toString().equals("")) {
                Toast.makeText(this, "O campo CEP está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtCadastroLogradoutro.getText().toString().equals("")) {
                Toast.makeText(this, "O campo logradouro está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtCadastroBairro.getText().toString().equals("")) {
                Toast.makeText(this, "O campo bairro está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtCadastroCidade.getText().toString().equals("")) {
                Toast.makeText(this, "O campo cidade está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtCadastroComplemento.getText().toString().equals("")) {
                Toast.makeText(this, "O campo complemento está vazio.", Toast.LENGTH_SHORT).show();

            } else {

//                imgFotoPessoal.buildDrawingCache();
//                Bitmap bitmap = imgFotoPessoal.getDrawingCache();
//
//                ByteArrayOutputStream bos = new ByteArrayOutputStream();
//                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
//                byte[] img = bos.toByteArray();
                cadastrarPassageiro();


            }


        }
    }

    public void viaCEP() {

        buscar_cep = false;

        WebService ws = new WebService(
                getString(R.string.servidor),
                "buscar_cep?cep=" + edtCadastroCEP.getText().toString(),
                "GET",
                null,
                "",
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores são: \n" + objeto);

                try {

                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");

                    JSONObject arrayInterno = objeto.getJSONObject("endereco");

                    String cep = arrayInterno.getString("cep");
                    String logradouro = arrayInterno.getString("logradouro");
                    String complemento = arrayInterno.getString("complemento");
                    String bairro = arrayInterno.getString("bairro");
                    String cidade = arrayInterno.getString("localidade");
                    String uf = arrayInterno.getString("uf");


                    System.out.println("STATUS = " + status);
                    System.out.println("RESULTADO = " + resultado);
                    System.out.println("CEP = " + cep);
                    System.out.println("Logradouro = " + logradouro);
                    System.out.println("COMPLEMENTO = " + complemento);
                    System.out.println("Bairro = " + bairro);
                    System.out.println("Cidade = " + cidade);
                    System.out.println("UF = " + uf);

                    edtCadastroCEP.setText(cep);
                    edtCadastroLogradoutro.setText(logradouro);
                    edtCadastroComplemento.setText(complemento);
                    edtCadastroBairro.setText(bairro);
                    edtCadastroCidade.setText(cidade);


                    for (int i = 1; i < camposEstados.length; i++) {

                        if (uf.equals(camposEstados[i])) {
                            ArrayAdapter<String> arrayUFAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, camposEstados);
                            arrayUFAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spnCadastroUF.setAdapter(arrayUFAdapter);
                            spnCadastroUF.setSelection(i);

                            i = i + 1;

                            System.out.println("O ID DA UF É = " + (i));
                            idUF = i;


                        }


                    }


                } catch (JSONException e) {

                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });
    }


    public void cadastrarPassageiro() {

        try {
            stringSHA1 = StringEncryption.SHA1(edtCadastroSenha.getText().toString());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        Map<String, String> parametros = new HashMap<>();
        parametros.put("nome_usuario", edtCadastroNome.getText().toString());
        parametros.put("email_usuario", edtCadastroEmail.getText().toString());
        parametros.put("telefone_usuario", edtCadastroTelefone.getText().toString());
        parametros.put("senha_usuario", stringSHA1);
        parametros.put("rg_usuario", edtCadastroCadastroRG.getText().toString());
        parametros.put("cpf_usuario", edtCadastroCPF.getText().toString());
        parametros.put("celular_usuario", edtCadastroCelular.getText().toString());
        parametros.put("logradouro_usuario", edtCadastroLogradoutro.getText().toString());
        parametros.put("cidade_usuario", edtCadastroCidade.getText().toString());
        parametros.put("fk_uf_usuario", idUF + "");
        parametros.put("bairro_usuario", edtCadastroBairro.getText().toString());
        parametros.put("cep_usuario", edtCadastroCEP.getText().toString());
        parametros.put("complemento_usuario", edtCadastroComplemento.getText().toString());
        parametros.put("num_residencia_usuario", edtCadastroNumero.getText().toString());
        parametros.put("fk_genero", idGenero + "");
        parametros.put("passageiro", base64Final);


        WebService ws = new WebService(
                getString(R.string.servidor),
                "cadastro_passageiro",
                "POST",
                parametros,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");
                    String id = objeto.getString("id");


                    System.out.println("STATUS DO CADASTRO PASSAGEIRO É = " + status);
                    System.out.println("RESULTADO DO CADASTRO PASSAGEIRO É = " + resultado);
                    System.out.println("ID DO CADASTRO PASSAGEIRO É = " + id);


                    if (status == 1) {
                        finish();
                        Toast.makeText(CadastroActivity.this, "Seu cadastro foi efetuado com sucesso.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CadastroActivity.this, "Erro ao cadastrar, tente novamente.", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }

    private void chamaImagem() {

        //MODAL VIEW TERMOS DE USO
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View dialogbox = CadastroActivity.this.getLayoutInflater().inflate(R.layout.dialog_camera_sd, null);

        ImageView camera = dialogbox.findViewById(R.id.btn_camera);
        ImageView galeria = dialogbox.findViewById(R.id.btn_galeria);

        ImageView btn_close = dialogbox.findViewById(R.id.btn_close);
//        final TextView tv_titulo = dialogbox.findViewById(R.id.tv_titulo_historico_procedimento);

        builder.setView(dialogbox);
        alertDialog_show = builder.create();
        alertDialog_show.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog_show.show();


        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(CadastroActivity.this.getBaseContext().getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    alertDialog_show.dismiss();
                }

            }
        });

        galeria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, RESULT_LOAD_IMG);
                alertDialog_show.dismiss();
            }
        });

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog_show.dismiss();
            }
        });

    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        Log.w("Entrei", "OnResult");

        if (resultCode == RESULT_OK && reqCode == RESULT_LOAD_IMG) {
            Log.w("Entrei", "Galeria");
            try {

                final Uri imageUri = data.getData();
                final Bitmap selectedImage = decodeUri(imageUri);

                InputStream inputStream = this.getContentResolver().openInputStream(imageUri);
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(inputStream, null, options);
                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;

                Log.w("Tamanhos da imagem", "Heigth = " + imageHeight + " : Width = " + imageWidth);
                if (imageHeight <= 150 || imageWidth <= 150) {
//                    ToolBox.ExibeMSG("Imagem muito pequena!", this);
                    Toast.makeText(this, "Imagem muito pequena, seleione outra.", Toast.LENGTH_SHORT).show();
                } else {
                    String base64 = encodeToBase64(selectedImage, Bitmap.CompressFormat.JPEG);

                    System.gc();

                    GuardaImagem(selectedImage);
                    System.out.println("O BASE 64 DA GALERIA É " + base64);

                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(CadastroActivity.this, "Você não selecionou uma foto.", Toast.LENGTH_LONG).show();

            }


        } else if (resultCode == RESULT_OK && reqCode == REQUEST_IMAGE_CAPTURE) {
            Log.w("Entrei", "Camera");

            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            String base64 = encodeToBase64(imageBitmap, Bitmap.CompressFormat.JPEG);

            System.gc();
            GuardaImagem(imageBitmap);

            System.out.println("O BASE 64 DA CAMERA É " + base64);
        } else {
            Toast.makeText(CadastroActivity.this, "Foto não Capturada.", Toast.LENGTH_LONG).show();
        }
        System.gc();


    }


    public static final int requiredSize = 200;

    public Bitmap decodeUri(Uri uri)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(CadastroActivity.this.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(CadastroActivity.this.getContentResolver().openInputStream(uri), null, o2);
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, 20, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }


    private void GuardaImagem(Bitmap bitmap) {

        String nova_imagem64 = Imagem_encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG);
        System.out.println("A STRING NOVA IMAGEM É " + nova_imagem64);
        base64Final = nova_imagem64;


        if (nova_imagem64.length() > 0) {
            Bitmap bitmap2 = Imagem_decodeBase64(nova_imagem64);
            imgFotoPessoal.setImageBitmap(bitmap2);
        }

    }

    public static String Imagem_encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, 20, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    /**
     * Função para Decodificar a imagem de String para Imagem Novamente
     */
    public static Bitmap Imagem_decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

}
