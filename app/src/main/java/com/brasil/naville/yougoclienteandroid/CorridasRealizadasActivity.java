package com.brasil.naville.yougoclienteandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.brasil.naville.yougoclienteandroid.Adapter.Adapter_Corridas_Realizadas;
import com.brasil.naville.yougoclienteandroid.Controller.Controller_Corridas_Realizadas;
import com.brasil.naville.yougoclienteandroid.WS.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CorridasRealizadasActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgVoltarCorridasRealizadas;


    ListView mylistview;
    AppShared sessao;


    ArrayList<Controller_Corridas_Realizadas> arrayListCorridasRealizadas = new ArrayList<>();
    final List<String> arrayIDCorridasRealizadas = new ArrayList<>();
    final List<String> arrayIdUsuario = new ArrayList<>();
    final List<String> arrayDataCorrida = new ArrayList<>();
    final List<String> arrayDataFimCorrida = new ArrayList<>();
    final List<String> arrayNomeUsuario = new ArrayList<>();
    final List<String> arrayEnderecoOrigem = new ArrayList<>();
    final List<String> arrayEnderecoDestino = new ArrayList<>();
    final List<String> arrayValorCorrida = new ArrayList<>();
    final List<String> arrayKMCorrida = new ArrayList<>();
    final List<String> arrayCarroCorrida = new ArrayList<>();
    final List<String> arrayPlacaCarroCorrida = new ArrayList<>();
    final List<String> arrayMotivoCancelamentoCorrida = new ArrayList<>();
    final List<String> arrayStatusCorrida = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_corridas_realizadas);


        imgVoltarCorridasRealizadas = findViewById(R.id.imgVoltarCorridasRealizadas);


        imgVoltarCorridasRealizadas.setOnClickListener(this);

        sessao = new AppShared(this);


        mylistview = findViewById(R.id.lstCorridasRealizadas);

        listar_corridas();


    }

    @Override
    public void onClick(View view) {
        if (view == imgVoltarCorridasRealizadas) {
            finish();
        }

    }


    public void listar_corridas() {


        Map<String, String> parametros = new HashMap<>();


        System.out.println(parametros);

        WebService ws = new WebService(
                getString(R.string.servidor),
                "listar_corridas",
                "GET",
                null,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");

                    JSONArray corridas = objeto.getJSONArray("corridas");


                    if (corridas.length() == 0) {
                        Toast.makeText(CorridasRealizadasActivity.this, "Você ainda não tem nenhuma corrida realizada, peça agora um YouGo.", Toast.LENGTH_SHORT).show();
                    }


                    Adapter_Corridas_Realizadas adapter_corridas_realizadas = new Adapter_Corridas_Realizadas(getApplicationContext(), R.layout.list_item_corridas_realizadas, arrayListCorridasRealizadas);

                    mylistview.setAdapter(adapter_corridas_realizadas);

                    for (int x = 0; x < corridas.length(); x++) {
                        JSONObject arrayInterno = corridas.getJSONObject(x);

                        Controller_Corridas_Realizadas hashMap = new Controller_Corridas_Realizadas();


                        String id_corrida = arrayInterno.getString("id_corrida");
                        String id_usuario = arrayInterno.getString("id_usuario");
                        String data_corrida_inicio = arrayInterno.getString("data_corrida_inicio");
                        String data_corrida_fim = arrayInterno.getString("data_corrida_fim");
                        String nome_usuario = arrayInterno.getString("nome_usuario");
                        String endereco_origem = arrayInterno.getString("endereco_origem");
                        String endereco_destino = arrayInterno.getString("endereco_destino");
                        String valor_corrida = arrayInterno.getString("valor_corrida");
                        String km_corrida = arrayInterno.getString("km_corrida");
                        String carro = arrayInterno.getString("carro");
                        String placa_carro_motorista = arrayInterno.getString("placa_carro_motorista");
                        String motivo_cancelamento = arrayInterno.getString("motivo_cancelamento");
                        String status_corrida = arrayInterno.getString("status_corrida");


                        hashMap.put(Controller_Corridas_Realizadas.id_corrida, id_corrida);
                        hashMap.put(Controller_Corridas_Realizadas.data_corrida_inicio, data_corrida_inicio);
                        hashMap.put(Controller_Corridas_Realizadas.valor_corrida, valor_corrida);
                        hashMap.put(Controller_Corridas_Realizadas.endereco_origem, endereco_origem);
                        hashMap.put(Controller_Corridas_Realizadas.endereco_destino, endereco_destino);
                        hashMap.put(Controller_Corridas_Realizadas.nome_usuario, nome_usuario);


                        System.out.println("STATUS DA LISTA DE VIAGENS É = " + status);
                        System.out.println("RESULTADO DA LISTA DE VIAGENS É = " + resultado);
                        System.out.println("ID DA CORRIDA = " + id_corrida);
                        System.out.println("ID DO USUÁRIO É = " + id_usuario);
                        System.out.println("DATA DO INICIO DA CORRIDA É = " + data_corrida_inicio);
                        System.out.println("DATA DO FIM DA CORRIDA É = " + data_corrida_fim);
                        System.out.println("NOME DO USUARIO É = " + nome_usuario);
                        System.out.println("ENDEREÇO DE ORIGEM É = " + endereco_origem);
                        System.out.println("ENDEREÇO DE DESTINO É = " + endereco_destino);
                        System.out.println("VALOR DA CORRIDA É = " + valor_corrida);
                        System.out.println("KM DA CORRIDA É = " + km_corrida);
                        System.out.println("O CARRO É = " + carro);
                        System.out.println("A PLACA DO CARRO É = " + placa_carro_motorista);
                        System.out.println("O MOTIVO DO CANCELAMENTO É = " + motivo_cancelamento);
                        System.out.println("O STATUS DA CORRIDA É = " + status_corrida);


                        arrayIdUsuario.add(id_usuario);
                        arrayIDCorridasRealizadas.add(id_corrida);
                        arrayDataCorrida.add(data_corrida_inicio);
                        arrayDataFimCorrida.add(data_corrida_fim);
                        arrayNomeUsuario.add(nome_usuario);
                        arrayEnderecoOrigem.add(endereco_origem);
                        arrayEnderecoDestino.add(endereco_destino);
                        arrayValorCorrida.add(valor_corrida);
                        arrayKMCorrida.add(km_corrida);
                        arrayCarroCorrida.add(carro);
                        arrayPlacaCarroCorrida.add(placa_carro_motorista);
                        arrayMotivoCancelamentoCorrida.add(motivo_cancelamento);
                        arrayStatusCorrida.add(status_corrida);


                        System.out.println("O ARRAY DAS CORRIDAS REALIZADAS É = " + arrayListCorridasRealizadas.toString());
                        arrayListCorridasRealizadas.add(hashMap);

                        mylistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                System.out.println("O INDEX DAS CORRIDAS REALIZADAS É = " + position);


                                sessao.setIDCorridaRealizada(arrayIDCorridasRealizadas.get(position));
                                sessao.setDataCorridaRealizada(arrayDataCorrida.get(position));
                                sessao.setValorCorridaRealizada(arrayValorCorrida.get(position));
                                sessao.setEnderecoOrigemCorridaRealizada(arrayEnderecoOrigem.get(position));
                                sessao.setEnderecoDestinoCorridaRealizada(arrayEnderecoDestino.get(position));
                                sessao.setNomeUsuarioCorridaRealizada(arrayNomeUsuario.get(position));
                                sessao.setIdUsuarioCorridaRealizada(arrayIdUsuario.get(position));
                                sessao.setDataFimCorridaRealizada(arrayDataFimCorrida.get(position));
                                sessao.setKmCorridaRealizada(arrayKMCorrida.get(position));
                                sessao.setCarroCorridaRealizada(arrayCarroCorrida.get(position));
                                sessao.setPlacaCarroCorridaRealizada(arrayPlacaCarroCorrida.get(position));
                                sessao.setMotivoCancelamentoCorridaRealizada(arrayMotivoCancelamentoCorrida.get(position));
                                sessao.setStatusCorridaRealizada(arrayStatusCorrida.get(position));


                                System.out.println("O ID DO INDEX DA CORRIDA É = " + arrayIDCorridasRealizadas.get(position));

                                Intent intent = new Intent(CorridasRealizadasActivity.this, DetalheCorridaRealizadaActivity.class);
                                startActivity(intent);
                            }
                        });
                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }
}
