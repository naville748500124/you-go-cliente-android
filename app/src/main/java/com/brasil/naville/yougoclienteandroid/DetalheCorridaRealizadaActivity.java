package com.brasil.naville.yougoclienteandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DetalheCorridaRealizadaActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgVoltarDetalheCorridasRealizadas;
    AppShared sessao;
    TextView txtIdCorridaRealizada;
    TextView txtDataInicialCorridaRealizada;
    TextView txtDataFinalCorridaRealizada;
    TextView txtCarroCorridaRealizada;
    TextView txtPlacaCarroCorridaRealizada;
    TextView txtMotoristaCorridaRealizada;
    TextView txtDeEdenrecoOrigemCorridaRealizada;
    TextView txtParaEnderecoDestinoCorridaRealizada;
    TextView txtDistanciaCorridaRealizada;
    TextView txtValorTotalCorridaRealizada;
    Button btnCustoDaCorrida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_corrida_realizada);

        sessao = new AppShared(this);


        txtIdCorridaRealizada = findViewById(R.id.txtIdCorridaRealizada);
        txtDataInicialCorridaRealizada = findViewById(R.id.txtDataInicialCorridaRealizada);
        txtDataFinalCorridaRealizada = findViewById(R.id.txtDataFinalCorridaRealizada);
        txtCarroCorridaRealizada = findViewById(R.id.txtCarroCorridaRealizada);
        txtPlacaCarroCorridaRealizada = findViewById(R.id.txtPlacaCarroCorridaRealizada);
        txtMotoristaCorridaRealizada = findViewById(R.id.txtMotoristaCorridaRealizada);
        txtDeEdenrecoOrigemCorridaRealizada = findViewById(R.id.txtDeEdenrecoOrigemCorridaRealizada);
        txtParaEnderecoDestinoCorridaRealizada = findViewById(R.id.txtParaEnderecoDestinoCorridaRealizada);
        txtDistanciaCorridaRealizada = findViewById(R.id.txtDistanciaCorridaRealizada);
        txtValorTotalCorridaRealizada = findViewById(R.id.txtValorTotalCorridaRealizada);
        btnCustoDaCorrida = findViewById(R.id.btnCustoDaCorrida);


        txtIdCorridaRealizada.setText(sessao.getIDCorridaRealizada());
        txtDataInicialCorridaRealizada.setText(sessao.getDataCorridaRealizada());
        txtDataFinalCorridaRealizada.setText(sessao.getDataFimCorridaRealizada());
        txtCarroCorridaRealizada.setText(sessao.getCarroCorridaRealizada());
        txtPlacaCarroCorridaRealizada.setText(sessao.getPlacaCarroCorridaRealizada());
        txtMotoristaCorridaRealizada.setText(sessao.getNomeUsuarioCorridaRealizada());
        txtDeEdenrecoOrigemCorridaRealizada.setText(sessao.getEnderecoOrigemCorridaRealizada());
        txtParaEnderecoDestinoCorridaRealizada.setText(sessao.getEnderecoDestinoCorridaRealizada());
        txtDistanciaCorridaRealizada.setText(sessao.getKmCorridaRealizada());
        txtValorTotalCorridaRealizada.setText(sessao.getValorCorridaRealizada());


        imgVoltarDetalheCorridasRealizadas = findViewById(R.id.imgVoltarDetalheCorridasRealizadas);


        imgVoltarDetalheCorridasRealizadas.setOnClickListener(this);
        btnCustoDaCorrida.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == imgVoltarDetalheCorridasRealizadas) {
            finish();
        }
        if (view == btnCustoDaCorrida) {

            Intent intent = new Intent(this, DetalhesDaCorrida2Activity.class);
            startActivity(intent);

        }

    }
}
