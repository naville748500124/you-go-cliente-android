package com.brasil.naville.yougoclienteandroid;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class DetalhesDaCorrida2Activity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgVoltarDetalhesDaCorrida2;
    WebView webView;
    public final String GlobalUrl = "http://google.com.br/";
    AppShared sessao;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_da_corrida2);


        webView = findViewById(R.id.webView1);

        sessao = new AppShared(this);


        webView = findViewById(R.id.webView1);
        loadWebViewLoad(webView);


        imgVoltarDetalhesDaCorrida2 = findViewById(R.id.imgVoltarDetalhesDaCorrida2);


        imgVoltarDetalhesDaCorrida2.setOnClickListener(this);


    }

    private void loadWebViewLoad(WebView webview) {
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setSupportMultipleWindows(true);
        webview.setWebViewClient(new WebViewClient());
        webview.setWebChromeClient(new WebChromeClient());
        webview.loadUrl("http://devnaville-br2.16mb.com/yougo/controller_webservice/comprovante_corrida?fk_corrida=" + sessao.getIDCorridaRealizada());
    }

    @Override
    public void onClick(View v) {
        if (v == imgVoltarDetalhesDaCorrida2) {
            finish();
        }
    }
}
