package com.brasil.naville.yougoclienteandroid;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brasil.naville.yougoclienteandroid.Util.Mask;
import com.brasil.naville.yougoclienteandroid.WS.WebService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EditarPagamentoActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView imgVoltarEditarPagamento;
    EditText edtEditarNumeroCartao;
    EditText edtEditarNomeCartao;
    EditText edtEditarDataCartao;
    EditText edtEditarCVVCartao;
    Button btnEditarCartao;
    AppShared sessao;
    TextView pickerDataNascimentoEditarCartao;
    public int dia, mes, ano;
    public String data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_pagamento);

        imgVoltarEditarPagamento = findViewById(R.id.imgVoltarEditarPagamento);
        edtEditarNomeCartao = findViewById(R.id.edtEditarNomeCartao);
        edtEditarNumeroCartao = findViewById(R.id.edtEditarNumeroCartao);
        edtEditarDataCartao = findViewById(R.id.edtEditarDataCartao);
        edtEditarCVVCartao = findViewById(R.id.edtEditarCVVCartao);
        btnEditarCartao = findViewById(R.id.btnEditarCartao);
        pickerDataNascimentoEditarCartao = findViewById(R.id.pickerDataNascimentoEditarCartao);


        edtEditarNomeCartao.setOnClickListener(this);
        edtEditarNumeroCartao.setOnClickListener(this);
        edtEditarDataCartao.setOnClickListener(this);
        edtEditarCVVCartao.setOnClickListener(this);
        btnEditarCartao.setOnClickListener(this);
        imgVoltarEditarPagamento.setOnClickListener(this);
        pickerDataNascimentoEditarCartao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                atualizarData();
            }
        });


        TextWatcher dataMask = Mask.insert("####/##", edtEditarDataCartao);
        edtEditarDataCartao.addTextChangedListener(dataMask);

        TextWatcher numeroCartaoMask = Mask.insert("####.####.####.####", edtEditarNumeroCartao);
        edtEditarNumeroCartao.addTextChangedListener(numeroCartaoMask);

        sessao = new AppShared(this);

        edtEditarNomeCartao.setText(sessao.getNomeCartao());
        edtEditarNumeroCartao.setText("");
    }

    @Override
    public void onClick(View v) {

        if (v == imgVoltarEditarPagamento) {
            Intent intent = new Intent(this, PagamentosActivity.class);
            startActivity(intent);
            finish();
        } else if (v == btnEditarCartao) {

            if (edtEditarNumeroCartao.getText().toString().equals("")) {
                Toast.makeText(this, "O campo número do cartão está vazio.", Toast.LENGTH_SHORT).show();
            } else if (edtEditarNomeCartao.getText().toString().equals("")) {
                Toast.makeText(this, "O campo nome está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtEditarDataCartao.getText().toString().equals("")) {
                Toast.makeText(this, "O campo data de epiração está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtEditarCVVCartao.getText().toString().equals("")) {
                Toast.makeText(this, "O campo CVV está vazio.", Toast.LENGTH_SHORT).show();

            } else {
                editarFomarPagamento();
            }


        }

    }

    public void atualizarData() {

        final DatePickerDialog datePickerDialog = new DatePickerDialog(EditarPagamentoActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                monthOfYear = monthOfYear + 1;
                final String dataAtual = dayOfMonth + "/" + monthOfYear + "/" + year;
                pickerDataNascimentoEditarCartao.setText(dayOfMonth + "/" + monthOfYear + "/" + year);
                data = year + "-" + monthOfYear + "-" + dayOfMonth;

                System.out.println("DATA ATUAL = " + dataAtual);


            }
        }, ano, mes - 1, dia);
        datePickerDialog.getDatePicker();
        datePickerDialog.show();
    }



    public void editarFomarPagamento() {


        Map<String, String> parametros = new HashMap<>();
        parametros.put("id_pagamento", sessao.getIDPagamento());
        parametros.put("nome_cartao", edtEditarNomeCartao.getText().toString());
        parametros.put("numero_cartao", edtEditarNumeroCartao.getText().toString());
        parametros.put("data_vencimento_cartao", edtEditarDataCartao.getText().toString().replace("/", "" + "01"));
        parametros.put("cvv_cartao", edtEditarCVVCartao.getText().toString());
        parametros.put("data_nascimento", data.replace("/", ""));

        WebService ws = new WebService(
                getString(R.string.servidor),
                "editar_forma_pagamento",
                "POST",
                parametros,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");


                    System.out.println("STATUS DA EDIÇÃO DO CADASTRO PASSAGEIRO É = " + status);
                    System.out.println("RESULTADO DA EDIÇÃO DO CADASTRO PASSAGEIRO É = " + resultado);


                    if (status == 1) {
                        Intent intent = new Intent(EditarPagamentoActivity.this, PagamentosActivity.class);
                        startActivity(intent);
                        finish();
                        Toast.makeText(EditarPagamentoActivity.this, "Sua forma de pagamento foi alterada com sucesso.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(EditarPagamentoActivity.this, "Erro aoalterar a forma de pagamento, tente novamente.", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }

}
