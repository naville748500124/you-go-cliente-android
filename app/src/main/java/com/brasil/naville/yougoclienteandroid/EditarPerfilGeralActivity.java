package com.brasil.naville.yougoclienteandroid;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.brasil.naville.yougoclienteandroid.Util.Mask;
import com.brasil.naville.yougoclienteandroid.WS.WebService;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class EditarPerfilGeralActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgVoltarEditarPerfil;
    Button btnEditarSalvar1;
    LinearLayout lnlEditar1;
    ScrollView scrEditar1;
    AppShared sessao;
    String stringSHA1;
    EditText edtEditarNome;
    Spinner spnEditarSexo;
    EditText edtEditarRG;
    EditText edtEditarCPF;
    EditText edtEditarEmail;
    EditText edtEditarConfirmarEmail;
    EditText edtEditarTelefone;
    EditText edtEditarCelular;
    EditText edtEditarCEP;
    EditText edtEditarLogradouro;
    EditText edtEditarBairro;
    EditText edtEditarCidade;
    Spinner spnEditarUF;
    EditText edtEditarComplemento;
    EditText edtEditarNumero;
    String email;
    String emailPattern;
    int idUF;
    int idGenero;
    String email2;
    String emailPattern2;
    String[] camposEstados = {"AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"};
    String[] camposSexo = {"Feminino", "Masculino"};
    Button btnEditarSenha;
    Button btnEditarFotoPessoal;
    LinearLayout lnlEditarFoto1;
    ImageView imgEditarFotoPessoal;
    Dialog alertDialog_show;
    String base64Final;
    private int RESULT_LOAD_IMG = 1;
    private int REQUEST_IMAGE_CAPTURE = 2;
    public static final int requiredSize = 200;
    String urlFoto;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil_geral);

        imgVoltarEditarPerfil = findViewById(R.id.imgVoltarEditarPerfil);
        btnEditarSalvar1 = findViewById(R.id.btnEditarSalvar1);
        lnlEditar1 = findViewById(R.id.lnlEditar1);
        scrEditar1 = findViewById(R.id.scrEditar1);

        edtEditarNome = findViewById(R.id.edtEditarNome);
        spnEditarSexo = findViewById(R.id.spnEditarSexo);
        edtEditarRG = findViewById(R.id.edtEditarRG);
        edtEditarCPF = findViewById(R.id.edtEditarCPF);
        edtEditarEmail = findViewById(R.id.edtEditarEmail);
        edtEditarConfirmarEmail = findViewById(R.id.edtEditarConfirmarEmail);
        edtEditarTelefone = findViewById(R.id.edtEditarTelefone);
        edtEditarCelular = findViewById(R.id.edtEditarCelular);
        edtEditarCEP = findViewById(R.id.edtEditarCEP);
        edtEditarLogradouro = findViewById(R.id.edtEditarLogradouro);
        edtEditarBairro = findViewById(R.id.edtEditarBairro);
        edtEditarCidade = findViewById(R.id.edtEditarCidade);
        spnEditarUF = findViewById(R.id.spnEditarUF);
        edtEditarComplemento = findViewById(R.id.edtEditarComplemento);
        edtEditarNumero = findViewById(R.id.edtEditarNumero);
        btnEditarSenha = findViewById(R.id.btnEditarSenha);
        btnEditarFotoPessoal = findViewById(R.id.btnEditarFotoPessoal);
        lnlEditarFoto1 = findViewById(R.id.lnlEditarFoto1);
        imgEditarFotoPessoal = findViewById(R.id.imgEditarFotoPessoal);


        ViewGroup.LayoutParams params = lnlEditarFoto1.getLayoutParams();
        params.height = 0;
        lnlEditarFoto1.setLayoutParams(params);


        imgVoltarEditarPerfil.setOnClickListener(this);
        btnEditarSalvar1.setOnClickListener(this);
        lnlEditar1.setOnClickListener(this);
        scrEditar1.setOnClickListener(this);
        btnEditarSenha.setOnClickListener(this);
        edtEditarNome.setOnClickListener(this);
        edtEditarRG.setOnClickListener(this);
        edtEditarCPF.setOnClickListener(this);
        edtEditarEmail.setOnClickListener(this);
        edtEditarConfirmarEmail.setOnClickListener(this);
        edtEditarTelefone.setOnClickListener(this);
        edtEditarCelular.setOnClickListener(this);
        edtEditarCEP.setOnClickListener(this);
        edtEditarLogradouro.setOnClickListener(this);
        edtEditarBairro.setOnClickListener(this);
        edtEditarCidade.setOnClickListener(this);
        edtEditarComplemento.setOnClickListener(this);
        edtEditarNumero.setOnClickListener(this);
        btnEditarFotoPessoal.setOnClickListener(this);
        lnlEditarFoto1.setOnClickListener(this);
        imgEditarFotoPessoal.setOnClickListener(this);


        ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, camposSexo);
        spinnerArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnEditarSexo.setAdapter(spinnerArrayAdapter2);


        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, camposEstados);
        spinnerArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnEditarUF.setAdapter(spinnerArrayAdapter1);


        sessao = new AppShared(this);


        urlFoto = "http://devnaville-br2.16mb.com/yougo/upload/passageiros/passageiro_" + sessao.getIdUsuario() + "/passageiro.png";
        Picasso.get().load(urlFoto).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(imgEditarFotoPessoal);

        preEditarCliente();


        //Tirar o teclado quando clicar fora
        lnlEditar1.setOnTouchListener(new View.OnTouchListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
                return false;
            }
        });

        //Tirar o teclado quando clicar fora
        scrEditar1.setOnTouchListener(new View.OnTouchListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
                return false;
            }
        });

        for (int i = 1; i < camposSexo.length; i++) {
            spnEditarSexo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 0) {
                        idGenero = 81;
                        System.out.println("O ID FO GENERO SELECIONADO É = " + idGenero);
                    } else {
                        idGenero = 82;
                        System.out.println("O ID DO GENERO SELECIONADO É = " + idGenero);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }


        edtEditarCEP.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    viaCEP();
                }
            }
        });


        TextWatcher telefoneMask = Mask.insert("(##)####-####", edtEditarTelefone);
        edtEditarTelefone.addTextChangedListener(telefoneMask);

        TextWatcher celularMask = Mask.insert("(##)#####-####", edtEditarCelular);
        edtEditarCelular.addTextChangedListener(celularMask);

        TextWatcher cpfMask = Mask.insert("###.###.###-##", edtEditarCPF);
        edtEditarCPF.addTextChangedListener(cpfMask);


        TextWatcher cepMask = Mask.insert("#####-###", edtEditarCEP);
        edtEditarCEP.addTextChangedListener(cepMask);


        TextWatcher RGWatcher = Mask.insert("##.###.###-#", edtEditarRG, 10, InputType.TYPE_CLASS_NUMBER, InputType.TYPE_CLASS_TEXT);
        edtEditarRG.addTextChangedListener(RGWatcher);


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    }

    boolean blFoto1 = false;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View view) {
        if (view == imgVoltarEditarPerfil) {
            finish();
        } else if (view == btnEditarSenha) {
            Intent intent = new Intent(this, EditarSenhaActivity.class);
            startActivity(intent);

        } else if (view == btnEditarSalvar1) {
            email = edtEditarEmail.getText().toString().trim();
            emailPattern = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
            email2 = edtEditarEmail.getText().toString().trim();
            emailPattern2 = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";


            if (edtEditarNome.getText().toString().equals("")) {
                Toast.makeText(this, "O campo nome está vazio.", Toast.LENGTH_SHORT).show();
            } else if (edtEditarRG.getText().toString().equals("")) {
                Toast.makeText(this, "O campo RG está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtEditarCPF.getText().toString().equals("")) {
                Toast.makeText(this, "O campo CPF está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtEditarEmail.getText().toString().equals("")) {
                Toast.makeText(this, "O campo e-mail está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtEditarConfirmarEmail.getText().toString().equals("")) {
                Toast.makeText(this, "O campo confirmar e-mail está vazio.", Toast.LENGTH_SHORT).show();

            } else if (!email.matches(emailPattern)) {

                Toast.makeText(getApplicationContext(), "Digite um e-mail válido.", Toast.LENGTH_SHORT).show();


            } else if (!Objects.equals(edtEditarConfirmarEmail.getText().toString(), edtEditarEmail.getText().toString())) {

                Toast.makeText(getApplicationContext(), "O e-mail e a confirmação tem que ser iguais.", Toast.LENGTH_SHORT).show();
            } else if (edtEditarTelefone.getText().toString().equals("")) {
                Toast.makeText(this, "O campo telefone está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtEditarCelular.getText().toString().equals("")) {
                Toast.makeText(this, "O campo celular está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtEditarCEP.getText().toString().equals("")) {
                Toast.makeText(this, "O campo CEP está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtEditarLogradouro.getText().toString().equals("")) {
                Toast.makeText(this, "O campo logradouro está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtEditarBairro.getText().toString().equals("")) {
                Toast.makeText(this, "O campo bairro está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtEditarCidade.getText().toString().equals("")) {
                Toast.makeText(this, "O campo cidade está vazio.", Toast.LENGTH_SHORT).show();

            } else if (edtEditarComplemento.getText().toString().equals("")) {
                Toast.makeText(this, "O campo complemento está vazio.", Toast.LENGTH_SHORT).show();

            } else {

                editarPassageiro();
            }
        } else if (view == btnEditarFotoPessoal) {
            if (!blFoto1) {

                ViewGroup.LayoutParams params = lnlEditarFoto1.getLayoutParams();


                params.height = params.height + 300;

                lnlEditarFoto1.setLayoutParams(params);
                blFoto1 = true;

            } else {
                ViewGroup.LayoutParams params = lnlEditarFoto1.getLayoutParams();
                params.height = params.height - 300;
                lnlEditarFoto1.setLayoutParams(params);
                blFoto1 = false;


            }
        } else if (view == imgEditarFotoPessoal) {
            chamaImagem();
        }

    }


    public void preEditarCliente() {

//        try {
//            stringSHA1 = StringEncryption.SHA1(edtCadastroSenha.getText().toString());
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }


        Map<String, String> parametros = new HashMap<>();
        parametros.put("nome_usuario", edtEditarNome.getText().toString());


        WebService ws = new WebService(
                getString(R.string.servidor),
                "pre_editar_cliente",
                "GET",
                null,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");


                    System.out.println("STATUS DA PRÉ-EDIÇÃO DO PASSAGEIRO É = " + status);
                    System.out.println("RESULTADO DA PRÉ-EDIÇÃO DO PASSAGEIRO É = " + resultado);


                    JSONObject arrayInterno = objeto.getJSONObject("cliente");

                    String nome_usuario = arrayInterno.getString("nome_usuario");
                    String email_usuario = arrayInterno.getString("email_usuario");
                    String telefone_usuario = arrayInterno.getString("telefone_usuario");
                    String senha_usuario = arrayInterno.getString("senha_usuario");
                    String rg_usuario = arrayInterno.getString("rg_usuario");
                    String cpf_usuario = arrayInterno.getString("cpf_usuario");
                    String celular_usuario = arrayInterno.getString("celular_usuario");
                    String logradouro_usuario = arrayInterno.getString("logradouro_usuario");
                    String cidade_usuario = arrayInterno.getString("cidade_usuario");
                    String fk_uf_usuario = arrayInterno.getString("fk_uf_usuario");
                    String bairro_usuario = arrayInterno.getString("bairro_usuario");
                    String cep_usuario = arrayInterno.getString("cep_usuario");
                    String complemento_usuario = arrayInterno.getString("complemento_usuario");
                    String num_residencia_usuario = arrayInterno.getString("num_residencia_usuario");
                    String fk_genero = arrayInterno.getString("fk_genero");


                    System.out.println("O NOME DO USUÁRIO É = " + nome_usuario);
                    System.out.println("O E-MAIL DO USUÁRIO É = " + email_usuario);
                    System.out.println("O TELEFONE DO USUÁRIO É = " + telefone_usuario);
                    System.out.println("A SENHA DO USUÁRIO É = " + senha_usuario);
                    System.out.println("O RG DO USUÁRIO É = " + rg_usuario);
                    System.out.println("O CPF DO USUÁRIO É = " + cpf_usuario);
                    System.out.println("O CELULAR DO USUÁRIO É = " + celular_usuario);
                    System.out.println("O LOGRADOURO DO USUÁRIO É = " + logradouro_usuario);
                    System.out.println("A CIDADE DO USUÁRIO É = " + cidade_usuario);
                    System.out.println("O ID DO ESTADO DO USUÁRIO É = " + fk_uf_usuario);
                    System.out.println("O BAIRRO DO USUÁRIO É = " + bairro_usuario);
                    System.out.println("O CEP DO USUÁRIO É = " + cep_usuario);
                    System.out.println("O COMPLEMENTO DO USUÁRIO É = " + complemento_usuario);
                    System.out.println("O NÚMERO DO USUÁRIO É = " + num_residencia_usuario);
                    System.out.println("O ID DO GENERO DO USUÁRIO É = " + fk_genero);

                    if (nome_usuario.equals("null") || nome_usuario.equals("")) {
                        edtEditarNome.setText("");
                        if (email_usuario.equals("null") || email_usuario.equals("")) {
                            edtEditarEmail.setText("");
                            edtEditarConfirmarEmail.setText("");

                            if (telefone_usuario.equals("null") || telefone_usuario.equals("")) {
//                                edtEditarTelefone.setText("");

                                if (senha_usuario.equals("null") || senha_usuario.equals("")) {


                                    if (rg_usuario.equals("null") || rg_usuario.equals("")) {
                                        edtEditarRG.setText("");

                                        if (cpf_usuario.equals("null") || cpf_usuario.equals("")) {
                                            edtEditarCPF.setText("");

                                            if (celular_usuario.equals("null") || celular_usuario.equals("")) {
                                                edtEditarCelular.setText("");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {


                        edtEditarNome.setText(nome_usuario);
                        edtEditarEmail.setText(email_usuario);
                        edtEditarConfirmarEmail.setText(email_usuario);
                        edtEditarTelefone.setText(telefone_usuario);
                        edtEditarRG.setText(rg_usuario);
                        edtEditarCPF.setText(cpf_usuario);
                        edtEditarCelular.setText(celular_usuario);
                        edtEditarLogradouro.setText(logradouro_usuario);
                        edtEditarCidade.setText(cidade_usuario);
                        edtEditarBairro.setText(bairro_usuario);
                        edtEditarCEP.setText(cep_usuario);
                        edtEditarComplemento.setText(complemento_usuario);
                        edtEditarNumero.setText(num_residencia_usuario);

                        if (fk_genero.equals("81")) {
                            spnEditarSexo.setSelection(0);

                        } else {
                            spnEditarSexo.setSelection(1);
                        }

                        if (fk_uf_usuario.equals("1")) {
                            spnEditarUF.setSelection(0);
                            idUF = 1;
                        } else {
                            if (!fk_uf_usuario.equals("null")) {
                                spnEditarUF.setSelection(Integer.parseInt(fk_uf_usuario) - 1);
                                idUF = Integer.parseInt(fk_uf_usuario);
                            }

                        }
                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }


    public void viaCEP() {

        WebService ws = new WebService(
                getString(R.string.servidor),
                "buscar_cep?cep=" + edtEditarCEP.getText().toString(),
                "GET",
                null,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores são: \n" + objeto);

                try {

                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");

                    JSONObject arrayInterno = objeto.getJSONObject("endereco");

                    String cep = arrayInterno.getString("cep");
                    String logradouro = arrayInterno.getString("logradouro");
                    String complemento = arrayInterno.getString("complemento");
                    String bairro = arrayInterno.getString("bairro");
                    String cidade = arrayInterno.getString("localidade");
                    String uf = arrayInterno.getString("uf");


                    System.out.println("STATUS = " + status);
                    System.out.println("RESULTADO = " + resultado);
                    System.out.println("CEP = " + cep);
                    System.out.println("Logradouro = " + logradouro);
                    System.out.println("COMPLEMENTO = " + complemento);
                    System.out.println("Bairro = " + bairro);
                    System.out.println("Cidade = " + cidade);
                    System.out.println("UF = " + uf);

                    edtEditarCEP.setText(cep);
                    edtEditarLogradouro.setText(logradouro);
                    edtEditarComplemento.setText(complemento);
                    edtEditarBairro.setText(bairro);
                    edtEditarCidade.setText(cidade);


                    for (int i = 1; i < camposEstados.length; i++) {

                        if (uf.equals(camposEstados[i])) {
                            ArrayAdapter<String> arrayUFAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, camposEstados);
                            arrayUFAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spnEditarUF.setAdapter(arrayUFAdapter);
                            spnEditarUF.setSelection(i);

                            i = i + 1;

                            System.out.println("O ID DA UF É = " + (i));
                            idUF = i;


                        }


                    }


                } catch (JSONException e) {

                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });
    }

    public void editarPassageiro() {


        Map<String, String> parametros = new HashMap<>();
        parametros.put("nome_usuario", edtEditarNome.getText().toString());
        parametros.put("email_usuario", edtEditarEmail.getText().toString());
        parametros.put("telefone_usuario", edtEditarTelefone.getText().toString());
//        parametros.put("senha_usuario", stringSHA1);
        parametros.put("rg_usuario", edtEditarRG.getText().toString());
        parametros.put("cpf_usuario", edtEditarCPF.getText().toString());
        parametros.put("celular_usuario", edtEditarCelular.getText().toString());
        parametros.put("logradouro_usuario", edtEditarLogradouro.getText().toString());
        parametros.put("cidade_usuario", edtEditarCidade.getText().toString());
        parametros.put("fk_uf_usuario", idUF + "");
        parametros.put("bairro_usuario", edtEditarBairro.getText().toString());
        parametros.put("cep_usuario", edtEditarCEP.getText().toString());
        parametros.put("complemento_usuario", edtEditarComplemento.getText().toString());
        parametros.put("num_residencia_usuario", edtEditarNumero.getText().toString());
        parametros.put("fk_genero", idGenero + "");
        if (base64Final != null) {
            if (!base64Final.equals("")) {
                parametros.put("passageiro", base64Final);
            }
        }
        WebService ws = new WebService(
                getString(R.string.servidor),
                "editar_passageiro",
                "POST",
                parametros,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");


                    System.out.println("STATUS DA EDIÇÃO DO CADASTRO PASSAGEIRO É = " + status);
                    System.out.println("RESULTADO DA EDIÇÃO DO CADASTRO PASSAGEIRO É = " + resultado);


                    if (status == 1) {
                        finish();
                        Toast.makeText(EditarPerfilGeralActivity.this, "Seu cadastro foi efetuado com sucesso.", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(EditarPerfilGeralActivity.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(EditarPerfilGeralActivity.this, "Erro ao cadastrar, tente novamente.", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }

    private void chamaImagem() {

        //MODAL VIEW TERMOS DE USO
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View dialogbox = EditarPerfilGeralActivity.this.getLayoutInflater().inflate(R.layout.dialog_camera_sd, null);

        ImageView camera = dialogbox.findViewById(R.id.btn_camera);
        ImageView galeria = dialogbox.findViewById(R.id.btn_galeria);

        ImageView btn_close = dialogbox.findViewById(R.id.btn_close);
//        final TextView tv_titulo = dialogbox.findViewById(R.id.tv_titulo_historico_procedimento);

        builder.setView(dialogbox);
        alertDialog_show = builder.create();
        alertDialog_show.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        alertDialog_show.show();


        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(EditarPerfilGeralActivity.this.getBaseContext().getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                    alertDialog_show.dismiss();
                }

            }
        });

        galeria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, RESULT_LOAD_IMG);
                alertDialog_show.dismiss();
            }
        });

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog_show.dismiss();
            }
        });

    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        Log.w("Entrei", "OnResult");

        if (resultCode == RESULT_OK && reqCode == RESULT_LOAD_IMG) {
            Log.w("Entrei", "Galeria");
            try {

                final Uri imageUri = data.getData();
                final Bitmap selectedImage = decodeUri(imageUri);

                InputStream inputStream = this.getContentResolver().openInputStream(imageUri);
                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(inputStream, null, options);
                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;

                Log.w("Tamanhos da imagem", "Heigth = " + imageHeight + " : Width = " + imageWidth);
                if (imageHeight <= 150 || imageWidth <= 150) {
//                    ToolBox.ExibeMSG("Imagem muito pequena!", this);
                    Toast.makeText(this, "Imagem muito pequena, seleione outra.", Toast.LENGTH_SHORT).show();
                } else {
                    String base64 = encodeToBase64(selectedImage, Bitmap.CompressFormat.JPEG);

                    System.gc();

                    GuardaImagem(selectedImage);
                    System.out.println("O BASE 64 DA GALERIA É " + base64);

                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(EditarPerfilGeralActivity.this, "Você não selecionou uma foto.", Toast.LENGTH_LONG).show();

            }


        } else if (resultCode == RESULT_OK && reqCode == REQUEST_IMAGE_CAPTURE) {
            Log.w("Entrei", "Camera");

            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            String base64 = encodeToBase64(imageBitmap, Bitmap.CompressFormat.JPEG);

            System.gc();
            GuardaImagem(imageBitmap);

            System.out.println("O BASE 64 DA CAMERA É " + base64);
        } else {
            Toast.makeText(EditarPerfilGeralActivity.this, "Foto não Capturada.", Toast.LENGTH_LONG).show();
        }
        System.gc();


    }


    public Bitmap decodeUri(Uri uri)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(EditarPerfilGeralActivity.this.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(EditarPerfilGeralActivity.this.getContentResolver().openInputStream(uri), null, o2);
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat
            compressFormat) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, 20, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }


    private void GuardaImagem(Bitmap bitmap) {

        String nova_imagem64 = Imagem_encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG);
        System.out.println("A STRING NOVA IMAGEM É " + nova_imagem64);
        base64Final = nova_imagem64;


        if (nova_imagem64.length() > 0) {
            Bitmap bitmap2 = Imagem_decodeBase64(nova_imagem64);
            imgEditarFotoPessoal.setImageBitmap(bitmap2);
        }

    }

    public static String Imagem_encodeToBase64(Bitmap
                                                       image, Bitmap.CompressFormat compressFormat) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, 20, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    /**
     * Função para Decodificar a imagem de String para Imagem Novamente
     */
    public static Bitmap Imagem_decodeBase64(String input) {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

}