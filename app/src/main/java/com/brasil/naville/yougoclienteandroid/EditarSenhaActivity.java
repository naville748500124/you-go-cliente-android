package com.brasil.naville.yougoclienteandroid;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.brasil.naville.yougoclienteandroid.Util.StringEncryption;
import com.brasil.naville.yougoclienteandroid.WS.WebService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class EditarSenhaActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgVoltarEditarSenha;
    EditText edtEditarSenhaAtual;
    EditText edtEditarNovaSenha;
    EditText edtEditarConfirmarNovaSenha;
    Button btnConfirmarEditarSenha;
    String stringSHA1;
    AppShared sessao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_senha);

        imgVoltarEditarSenha = findViewById(R.id.imgVoltarEditarSenha);
        edtEditarSenhaAtual = findViewById(R.id.edtEditarSenhaAtual);
        edtEditarNovaSenha = findViewById(R.id.edtEditarNovaSenha);
        edtEditarConfirmarNovaSenha = findViewById(R.id.edtEditarConfirmarNovaSenha);
        btnConfirmarEditarSenha = findViewById(R.id.btnConfirmarEditarSenha);

        sessao = new AppShared(this);


        imgVoltarEditarSenha.setOnClickListener(this);
        edtEditarSenhaAtual.setOnClickListener(this);
        edtEditarNovaSenha.setOnClickListener(this);
        edtEditarConfirmarNovaSenha.setOnClickListener(this);
        btnConfirmarEditarSenha.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        if (v == imgVoltarEditarSenha) {
            finish();
        } else if (v == btnConfirmarEditarSenha) {

            try {
                stringSHA1 = StringEncryption.SHA1(edtEditarSenhaAtual.getText().toString());
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }
        if (edtEditarSenhaAtual.getText().toString().equals(sessao.getSenhaMotorista())) {
            Toast.makeText(this, "Sua senha atual não está correta.", Toast.LENGTH_SHORT).show();


        } else if (edtEditarNovaSenha.length() <= 7) {
            Toast.makeText(this, "Sua senha precisa ter no mínimo 8 caracteres.", Toast.LENGTH_SHORT).show();
        } else if (!Objects.equals(edtEditarConfirmarNovaSenha.getText().toString(), edtEditarNovaSenha.getText().toString())) {
            Toast.makeText(getApplicationContext(), "A senha e a confirmação tem que ser iguais.", Toast.LENGTH_SHORT).show();
        } else if (edtEditarNovaSenha.getText().toString().equals("")) {
            Toast.makeText(this, "O campo nova senha está vazio.", Toast.LENGTH_SHORT).show();

        } else {
            editarPassageiro();

        }

    }

    public void editarPassageiro() {

        try {
            stringSHA1 = StringEncryption.SHA1(edtEditarNovaSenha.getText().toString());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        Map<String, String> parametros = new HashMap<>();

        parametros.put("senha_usuario", stringSHA1);

        WebService ws = new WebService(
                getString(R.string.servidor),
                "editar_passageiro",
                "POST",
                parametros,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");


                    System.out.println("STATUS DA EDIÇÃO DO CADASTRO PASSAGEIRO É = " + status);
                    System.out.println("RESULTADO DA EDIÇÃO DO CADASTRO PASSAGEIRO É = " + resultado);


                    if (status == 1) {
                        finish();
                        Toast.makeText(EditarSenhaActivity.this, "Sua senha foi editada com sucesso.", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(EditarSenhaActivity.this, MainActivity.class);
                        sessao.setSenhaMotorista(stringSHA1);
                        startActivity(intent);
                    } else {
                        Toast.makeText(EditarSenhaActivity.this, "Erro ao editar a senha, tente novamente.", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }
}
