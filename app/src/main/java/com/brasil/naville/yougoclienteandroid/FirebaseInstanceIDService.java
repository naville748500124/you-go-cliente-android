package com.brasil.naville.yougoclienteandroid;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
/**
 * Created by eduardodossantos on 06/11/2017.
 */

public class FirebaseInstanceIDService extends FirebaseInstanceIdService {



    @Override
    public void onTokenRefresh() {

        System.out.println("onTokenRefresh");

        String token = FirebaseInstanceId.getInstance().getToken();

        System.out.println("Token gerado: "+token);

        //registerToken(token);
    }

}