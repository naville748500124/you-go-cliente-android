package com.brasil.naville.yougoclienteandroid;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by eduardodossantos on 06/11/2017.
 */

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        System.out.println("onMessageReceived");

        System.out.println("O TITULO DA NOTIFICACAO É = " + remoteMessage.getData().get("title"));
        System.out.println("A MENSAGEM DA NOTIFICACAO É = " + remoteMessage.getData().get("message"));
        System.out.println("A MENSAGEM BRUTA É = " + remoteMessage.getData());


        showNotification(remoteMessage.getData().get("mensagem"), remoteMessage.getData().get("titulo"));

    }

    private void showNotification(String mensagem, String titulo) {

        System.out.println("showNotification");

        Intent i = new Intent(this, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        PendingIntent p = PendingIntent.getActivity(this, 0, new Intent(this, LoginActivity.class), 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setTicker(titulo);
        builder.setContentTitle(titulo);
        builder.setContentText(mensagem);
        builder.setSmallIcon(R.drawable.notification1);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.notification1));
        builder.setContentIntent(p);

        System.out.println("Título: " + titulo);

        NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();
        String[] descs = new String[]{titulo, mensagem};

        for (int num = 0; num < descs.length; num++) {
            style.addLine(descs[num]);
        }
        builder.setStyle(style);

        Notification n = builder.build();
        n.vibrate = new long[]{150, 300, 150, 600};
        n.flags = Notification.FLAG_AUTO_CANCEL;
        nm.notify(R.drawable.notification1, n);

        try {
            Uri som = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone toque = RingtoneManager.getRingtone(this, som);
            toque.play();
        } catch (Exception e) {
        }
    }


}
