package com.brasil.naville.yougoclienteandroid.Fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

public class FragmentDatePicker extends DialogFragment {

    public static final String MyDatePREFERENCES = "datePrefs";
    SharedPreferences sharedPreferences;
    DatePicker datePicker;
    Calendar calendar = Calendar.getInstance();

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        calendar.getTimeInMillis();
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);


        sharedPreferences = getActivity().getSharedPreferences(MyDatePREFERENCES, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        String dateDay = String.valueOf(dayOfMonth);
        String dateMonth = String.valueOf(month);
        String dateYear = String.valueOf(year);

        editor.putString("dateDay", dateDay);
        editor.putString("dateMonth", dateMonth);
        editor.putString("dateYear", dateYear);
        editor.putString("dataPedido", dateDay + dateMonth + dateYear);


        return new DatePickerDialog(getActivity(), (DatePickerDialog.OnDateSetListener) getActivity(), dayOfMonth, month, year);
    }

}
