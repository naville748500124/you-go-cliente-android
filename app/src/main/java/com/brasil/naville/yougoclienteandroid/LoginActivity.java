package com.brasil.naville.yougoclienteandroid;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.brasil.naville.yougoclienteandroid.Util.StringEncryption;
import com.brasil.naville.yougoclienteandroid.WS.WebService;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txtPolitica;
    TextView txtRestaurar;
    LinearLayout lnlLogin;
    TextView txtCadastrese;
    Button btnLogin;
    String id_recebida_usuario;
    String celular_recebida_usuario;
    AppShared sessao;
    EditText edtEmailLogin;
    EditText edtSenhaLogin;
    String stringSHA1;
    String email;
    String emailPattern;
    public static int APP_REQUEST_CODE = 99;
    View view;
    CallbackManager callbackManager;
    LoginButton loginButton;
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.CAMERA, Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    String token;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtPolitica = findViewById(R.id.txtPolitica);
        txtRestaurar = findViewById(R.id.txtRestaurar);
        lnlLogin = findViewById(R.id.lnlLogin);
        txtCadastrese = findViewById(R.id.txtCadastrese);
        btnLogin = findViewById(R.id.btnLogin);
        edtEmailLogin = findViewById(R.id.edtEmailLogin);
        edtSenhaLogin = findViewById(R.id.edtSenhaLogin);
        loginButton = findViewById(R.id.login_button);

//        Intent intent = new Intent(this, MainActivity.class);
//        startActivity(intent);
//        finish();


        sessao = new AppShared(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        token = FirebaseInstanceId.getInstance().getToken();
        System.out.println("O TOKEN DO FIREBASE É = " + token);


        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }


        /*STATUS 83 - BUSCANDO MOTORISTA
        STATUS 84 - AGUARDANDO MOTORISTA
        STATUS 85 - CORRIDA COMEÇOU
        STATUS 86 - CORRIDA FOI FINALIZADA COM SUCESSO
        STATUS 87 - PASSAGEIRO CANCELOU
        STATUS 88 - MOTORISTA CANCELOU*/


        if (sessao.getUltimaCorrida() != null) {
            if (sessao.getUltimaCorrida().equals("84")) {
                //RECUPERAR A ROTA
            }
        }


        txtPolitica.setOnClickListener(this);
        txtRestaurar.setOnClickListener(this);
        lnlLogin.setOnClickListener(this);
        txtCadastrese.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        loginButton.setOnClickListener(this);

        callbackManager = CallbackManager.Factory.create();

        callbackManager = CallbackManager.Factory.create();


        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        System.out.println("LOGADO PELO FACEBOOK");
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                    }

                    @Override
                    public void onCancel() {
                        System.out.println("LOGIN DO FACEBOOK CANCELADO");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        System.out.println("ERRO NO LOGIN DO FACEBOOK");
                    }
                });
    }

//        //Tirar o teclado quando clicar fora
//        lnlLogin.setOnTouchListener(new View.OnTouchListener() {
//            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
//                assert imm != null;
//                imm.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
//                return false;
//            }
//        });
//    }

    public void phoneLogin(View view) {
        final Intent intent = new Intent(LoginActivity.this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.CODE); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(
            final int requestCode,
            final int resultCode,
            final Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);


        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APP_REQUEST_CODE) { // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
                Toast.makeText(this, "ERRO NA SMS", Toast.LENGTH_SHORT).show();
            } else if (loginResult.wasCancelled()) {
                toastMessage = "SMS CANCELADA";
            } else {
                if (loginResult.getAccessToken() != null) {
                    toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();
                } else {
//                    toastMessage = String.format(
//                            "DEU CERTO",
//                            loginResult.getAuthorizationCode().substring(0, 10));
                    ativarViaSms();
                }

                // If you have an authorization code, retrieve it from
                // loginResult.getAuthorizationCode()
                // and pass it to your server and exchange it for an access token.

                // Success! Start your next activity...
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            }

            // Surface the result to your user in an appropriate way.
//            Toast.makeText(
//                    this,
//                    toastMessage,
//                    Toast.LENGTH_LONG)
//                    .show();
        }

        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                // Get Account Kit ID
                String accountKitId = account.getId();

                // Get phone number
                PhoneNumber phoneNumber = account.getPhoneNumber();
                if (phoneNumber != null) {
                    String phoneNumberString = phoneNumber.toString();
                }

                // Get email
                String email = account.getEmail();
            }

            @Override
            public void onError(final AccountKitError error) {
                // Handle Error
            }
        });
    }

    public void loginFace() {

        callbackManager = CallbackManager.Factory.create();
//        loginButton.setReadPermissions(Arrays.asList(idDoFace));
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                System.out.println("SUCESSO");


                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }

            @Override
            public void onCancel() {
                System.out.println("CANCELADO!");
            }

            @Override
            public void onError(FacebookException error) {
                System.out.println("ERRO");
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();

        //SE USUÁRIO ESTIVER LOGADO
        com.facebook.AccessToken accessToken = com.facebook.AccessToken.getCurrentAccessToken();

        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
//                    id_facebook = object.getString("id");
                    System.out.println("ID do Facebook = " + "teste");
//                    session.setIDFacebook(id_facebook);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id");
        request.setParameters(parameters);
        request.executeAsync();
    }


    @Override
    public void onClick(View v) {

        if (v == txtPolitica) {
            Intent intent = new Intent(this, PoliticaActivity.class);
            startActivity(intent);
        } else if (v == txtRestaurar) {
            Intent intent = new Intent(this, RestaurarSenhaActivity.class);
            startActivity(intent);
        } else if (v == txtCadastrese) {
            Intent intent = new Intent(this, CadastroActivity.class);
            startActivity(intent);
        } else if (v == btnLogin) {

            email = edtEmailLogin.getText().toString().trim();
            emailPattern = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

            if (!email.matches(emailPattern)) {
                Toast.makeText(this, "Digite um e-mail válido.", Toast.LENGTH_SHORT).show();


            } else if (edtSenhaLogin.getText().toString().trim().equals("")) {
                Toast.makeText(this, "O campo senha está vazio.", Toast.LENGTH_SHORT).show();


            } else {

                chamarWebServiceLogin();

            }
        } else if (view == loginButton) {
            loginFace();
        }


    }


    public void chamarWebServiceLogin() {

        Map<String, String> parametros = new HashMap<>();


        try {
            stringSHA1 = StringEncryption.SHA1(edtSenhaLogin.getText().toString().trim());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        sessao.setEmailMotorista(edtEmailLogin.getText().toString());
        sessao.setSenhaMotorista(stringSHA1);


        WebService ws = new WebService(
                getString(R.string.servidor),
                "login_usuario?tipo_acesso=3&token=" + token,
                "GET",
                null,
                edtEmailLogin.getText().toString() + ":" + stringSHA1,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");


                    if (status == 2) {
                        JSONObject arrayInternoSMS = objeto.getJSONObject("usuario");


                        String id_usuario = arrayInternoSMS.getString("id_usuario");
                        String celular_usuario = arrayInternoSMS.getString("celular_usuario");


                        id_recebida_usuario = id_usuario;
                        celular_recebida_usuario = celular_usuario;


                        System.out.println("O ID DO USUARIO É = " + id_usuario);
                        System.out.println("O CELULAR DO USUÁRIO É = " + celular_usuario);


                        phoneLogin(view);
                    }

                    switch (resultado) {
                        case "Dados incorretos ou perfil inativo!":
                            Toast.makeText(LoginActivity.this, "Dados incorretos ou perfil inativo!", Toast.LENGTH_SHORT).show();
                            break;
                        case "Perfil incorreto para essa chamada":
                            Toast.makeText(LoginActivity.this, "Esse é um perfilde motorista, entre com um perfil de passageiro.", Toast.LENGTH_SHORT).show();
                            break;
                        case "Perfil pendente de ativação por SMS!":
                            Toast.makeText(LoginActivity.this, "Perfil pendente de ativação por SMS! Ative agora por SMS.", Toast.LENGTH_SHORT).show();
                            break;
                        case "Perfil pendente de ativação por E-Mail!":
                            Toast.makeText(LoginActivity.this, "Perfil pendente de ativação por E-Mail! Verique o seu e-mail e ative sua conta.", Toast.LENGTH_SHORT).show();
                            break;
                        case "Perfil pendente de ativação por E-Mail!, falha no envio":
                            Toast.makeText(LoginActivity.this, "Falha no envio da ativação no e-mail, tente novamente.", Toast.LENGTH_SHORT).show();
                            break;
                        case "Perfil inativo ou bloqueado":
                            Toast.makeText(LoginActivity.this, "Perfil inativo ou bloqueado, contate o YouGo.", Toast.LENGTH_SHORT).show();
                            break;
                    }

                    JSONObject arrayInterno = objeto.getJSONObject("usuario");

                    String nome_usuario = arrayInterno.getString("nome_usuario");
                    String email_usuario = arrayInterno.getString("email_usuario");
                    String id_usuario = arrayInterno.getString("id_usuario");
                    String senha_usuario = arrayInterno.getString("senha_usuario");
                    String token_acesso = arrayInterno.getString("token_acesso");
                    String ultima_corrida = arrayInterno.getString("ultima_corrida");

                    JSONObject arrayInterno2 = objeto.getJSONObject("ajuda");

                    String tel_empresa = arrayInterno2.getString("tel_empresa");
                    String email_empresa = arrayInterno2.getString("email_empresa");


                    System.out.println("Status = " + status);
                    System.out.println("Resultado = " + resultado);
                    System.out.println("Nome do usuário = " + nome_usuario);
                    System.out.println("E-mail do usuário = " + email_usuario);
                    System.out.println("Senha do usuário = " + senha_usuario);
                    System.out.println("Telefone da empresa = " + tel_empresa);
                    System.out.println("E-mail da empresa = " + email_empresa);
                    System.out.println("O ID do usuário é = " + id_usuario);
                    System.out.println("TOKEN ACESSO  = " + token_acesso);
                    System.out.println("ÚLTIMA CORRIDA = " + ultima_corrida);

                    sessao.setUltimaCorrida(ultima_corrida);
                    sessao.setNomePassageiro(nome_usuario);
                    sessao.setIdUsuario(id_usuario);
                    sessao.setTelefoneAjuda(tel_empresa);
                    sessao.setEmailAjuda(email_empresa);
                    sessao.setTokenAcessoLogin(token_acesso);


                    if (status == 1) {
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), "Logado com sucesso.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(LoginActivity.this, resultado, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }

    public void ativarViaSms() {


        Map<String, String> parametros = new HashMap<>();
        parametros.put("id_usuario", id_recebida_usuario);


        WebService ws = new WebService(
                getString(R.string.servidor),
                "ativar_sms",
                "POST",
                parametros,
                sessao.getEmailMotorista() + ":" + sessao.getSenhaMotorista(),
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");


                    System.out.println("STATUS DO CADASTRO BANCO É = " + status);
                    System.out.println("RESULTADO DO CADASTRO BANCO É = " + resultado);


                    if (status == 1) {
//                        Toast.makeText(LoginActivity.this, "Dados bancários inseridos com sucesso!", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
//                        Toast.makeText(LoginActivity.this, "Erro ao cadastrar os dados bancários, tente novamente.", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


}
