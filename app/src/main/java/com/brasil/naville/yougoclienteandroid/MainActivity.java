package com.brasil.naville.yougoclienteandroid;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.brasil.naville.yougoclienteandroid.Animation.Remote.Constants;
import com.brasil.naville.yougoclienteandroid.Animation.Remote.IGoogleAPI;
import com.brasil.naville.yougoclienteandroid.Directions.DirectionsJSONParser;
import com.brasil.naville.yougoclienteandroid.Directions.FocusViews;
import com.brasil.naville.yougoclienteandroid.WS.WebService;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, View.OnClickListener {


    public List<LatLng> polylineList;
    public Marker marker;
    public float v;
    public Handler handler;
    public LatLng startPosition, endPosition;
    public int index, next;
    public String destitation;
    public PolylineOptions polylineOptions, blackPolylineOptions;
    public Polyline blackPolyline, greyPolyline;
    public LatLng minhaLocalizacao;
    public boolean tracou = false;

    public IGoogleAPI mService;


    private GoogleMap mMap;
    Location location;
    LocationManager locationManager;
    double latitude;
    double longitude;
    PlaceAutocompleteFragment autocompleteFragment;
    PlaceAutocompleteFragment autocompleteFragmentDestino;
    double latitudePartida;
    double longitudePartida;
    String stringPartida;
    ArrayList arrayMarkers = new ArrayList();
    MarkerOptions markerOptions, markerOptions2;
    double latitudeDestino;
    double longitudeDestino;
    String stringDestino;
    AppShared sessao;
    Button btnPedirYouGo;
    Button btnCancelarCorrida;
    String id_corrida;
    Button btnConcluirBloquinho;
    EditText bloquinho;
    String motivoCancelamento;
    TimerTask task = null;
    String pontualidade_motorista;
    String simpatia_motorista;
    String carro_motorista;
    String servico_motorista;
    String cidade_origem;
    ProgressDialog dialog;
    Button btnAvaliarMotorista;
    RatingBar ratingBarPontualidadeMotorista;
    RatingBar ratingBarSimpatiaMotorista;
    RatingBar ratingBarCarroMotorista;
    RatingBar ratingBarServicoMotorista;
    int rating1;
    int rating2;
    int rating3;
    int rating4;
    long animatioDuration = 1000;
    long animatioDuration2 = 500;
    LinearLayout lnlDadosMotorista, llValorAproximado;
    Button btnPedirYougo2;
    FrameLayout frmMain;
    EditText edtQuantidadeMalas;
    EditText edtQuantidadePassageiros;
    final List<String> arrayNumeroCartao = new ArrayList<>();
    final List<String> arrayIdCartao = new ArrayList<>();
    Spinner spnEscolhaPagamento;
    String cartaoSelecionado;
    String idCartaoSelecionado;
    ArrayAdapter arrayAdapterCartao;
    TextView txtNomeMenu;
    String urlFoto, url;
    CircleImageView fotoUsuario;
    String cidadeOrigem;
    FocusViews focusViews;
    Criteria criteria;
    String provider;
    Double lat, lng;
    String kmCorrida = "", cupomDigitado = "";
    TextView txtValorAproximado, txtTempoEstimado;
    EditText edtCupom;
    Button btnPedirCorrida;
    //    RelativeLayout rlCirculoPreto;
    String distanciaEmKMRota;
    public Marker mMarker;
    DownloadTask downloadTask;
    public LatLng latLng, origin, dest, latLngPassageiro, latLngMotorista, latLngDestino, latLngPartida, myCurrentLocation, myChangedLocation;
    public List<LatLng> listPosicoes = new ArrayList<>();
    public LatLng latLngP, latLngD;
    public LocationListener locationListener, locationListener2;

    @SuppressLint({"ClickableViewAccessibility", "MissingPermission"})
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        rlCirculoPreto = findViewById(R.id.rlCirculoPreto);
        focusViews = new FocusViews(getApplicationContext());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        btnPedirYouGo = findViewById(R.id.btnPedirYouGo);
        llValorAproximado = findViewById(R.id.llValorAproximado);
        btnCancelarCorrida = findViewById(R.id.btnCancelarCorrida);
        lnlDadosMotorista = findViewById(R.id.lnlDadosMotorista);
        frmMain = findViewById(R.id.frmMain);
        edtQuantidadeMalas = findViewById(R.id.edtQuantidadeMalas);
        edtQuantidadePassageiros = findViewById(R.id.edtQuantidadePassageiros);
        spnEscolhaPagamento = findViewById(R.id.spnEscolhaPagamento);
        btnPedirCorrida = findViewById(R.id.btnPedirCorrida);
        txtTempoEstimado = findViewById(R.id.txtTempoEstimado);

        lnlDadosMotorista.setOnClickListener(this);
        edtQuantidadeMalas.setOnClickListener(this);
        edtQuantidadePassageiros.setOnClickListener(this);
        llValorAproximado.setOnClickListener(this);


        frmMain.setOnClickListener(this);


        //Tirar o teclado quando clicar fora
        lnlDadosMotorista.setOnTouchListener(new View.OnTouchListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
                return false;
            }
        });


        sessao = new AppShared(this);

        Objects.requireNonNull(getSupportActionBar()).setTitle("");

        criteria = new Criteria();
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        if (locationManager != null) {
            provider = locationManager.getBestProvider(criteria, true);
            location = locationManager.getLastKnownLocation(provider);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 1, new MyLocationListener());
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);


        if (location != null) {
            lat = location.getLatitude();
            lng = location.getLongitude();

            myCurrentLocation = new LatLng(lat, lng);
            System.out.println("Location = " + myCurrentLocation.toString());

        }




        polylineList = new ArrayList<>();
        mapFragment.getMapAsync(this);


        mService = Constants.getGoogleApi();


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        txtValorAproximado = findViewById(R.id.txtValorAproximado);
        btnPedirYougo2 = findViewById(R.id.btnPedirYougo2);
        edtCupom = findViewById(R.id.edtCupom);


        btnPedirYougo2.setOnClickListener(this);


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.setItemIconTintList(null);
        navigationView.setItemTextColor(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.colorBlack)));
        View heaaderView = navigationView.getHeaderView(0);
        txtNomeMenu = heaaderView.findViewById(R.id.txtNomeMenu);
        fotoUsuario = heaaderView.findViewById(R.id.imgFotoPerfilMenu);


        txtNomeMenu.setText(sessao.getNomePassageiro());

        urlFoto = "http://devnaville-br2.16mb.com/yougo/upload/passageiros/passageiro_" + sessao.getIdUsuario() + "/passageiro.png";
        Picasso.get().load(urlFoto).memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE).into(fotoUsuario);

        System.out.println("A URL DA FOTO É = " + urlFoto);


        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);


        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                .build();


//        edtCupom.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                cupom = s.toString();
//                System.out.println("Digitando = "+cupom);
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });

//        btnPedirYouGo.setOnClickListener(this);
        llValorAproximado.setOnClickListener(this);
        btnCancelarCorrida.setOnClickListener(this);

        btnCancelarCorrida.setVisibility(View.GONE);

        autocompleteFragment.setFilter(typeFilter);
        autocompleteFragment.setHint("Endereço de partida");

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {


                stringPartida = place.getAddress().toString();
                latitudePartida = place.getLatLng().latitude;
                longitudePartida = place.getLatLng().longitude;
                System.out.println("O ENDEREÇO DA PARTIDA SELECIONADA É = " + stringPartida);


                Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());

                try {
                    List<Address> addresses = geocoder.getFromLocationName(stringPartida, 1);

                    System.out.println("ADDRESS = " + addresses.toString());

                    cidadeOrigem = addresses.get(0).getSubAdminArea();
                    sessao.setCidadeOrigem(cidadeOrigem);

                } catch (IOException e) {
                    e.printStackTrace();
                }


                sessao.setEnderecoOrigem(stringPartida);

                latLngPartida = new LatLng(latitudePartida, longitudePartida);
//                MarkerOptions markerOptions = new MarkerOptions();
//                markerOptions.position(latLngPartida);
//                mMap.addMarker(markerOptions);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngPartida, 18));

                sessao.setLatitudeOrigem(String.valueOf(latitudePartida));
                sessao.setLongitudeOrigem(String.valueOf(longitudePartida));

//                arrayMarkers.add(latLngPartida);
//                System.out.println("ARRAY MARKERS = "+arrayMarkers.toString());


                arrayMarkers.add(latLngPartida);

            }

            @Override
            public void onError(Status status) {


            }
        });

        btnPedirCorrida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listarFormasPagamento();
                descerInformacoesCorridaAnimation();
//                desenharRota();
                dadosMotoristaAnimation();

            }
        });

        autocompleteFragmentDestino = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment2);


        AutocompleteFilter typeFilterDestino = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                .build();


        autocompleteFragmentDestino.setFilter(typeFilterDestino);
        autocompleteFragmentDestino.setHint("Endereço do destino");

        autocompleteFragmentDestino.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place placeDestino) {


                stringDestino = placeDestino.getAddress().toString();
                latitudeDestino = placeDestino.getLatLng().latitude;
                longitudeDestino = placeDestino.getLatLng().longitude;
                System.out.println("O ENDEREÇO DO DESTINO SELECIONADO É = " + stringDestino);


                sessao.setLatitudeDestino(String.valueOf(latitudeDestino));
                sessao.setLongitudeDestino(String.valueOf(longitudeDestino));
                sessao.setEnderecoDestino(stringDestino);

                latLngDestino = new LatLng(latitudeDestino, longitudeDestino);
//                MarkerOptions markerOptions2 = new MarkerOptions();
//                markerOptions2.position(latLngDestino);
//                mMap.addMarker(markerOptions2);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngDestino, 18));


                arrayMarkers.add(latLngDestino);
                System.out.println("ARRAY MARKERS = " + arrayMarkers.toString());

                subirInformacoesCorridaAnimation();
                custo_corrida();


            }

            @Override
            public void onError(Status status) {

            }
        });


    }

    public void dadosMotoristaAnimation() {
        ObjectAnimator animatorY = ObjectAnimator.ofFloat(lnlDadosMotorista, "y", 0f);
        animatorY.setDuration(animatioDuration);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animatorY);
        animatorSet.start();

    }

    public void dadosMotoristaAnimation2() {
        ObjectAnimator animatorY = ObjectAnimator.ofFloat(lnlDadosMotorista, "y", 0f);
        animatorY.setDuration(animatioDuration);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animatorY);
        animatorSet.start();

    }

    public void botaoYouGoAnimation() {
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(btnPedirYouGo, "x", -1800f);
        animatorX.setDuration(animatioDuration2);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animatorX);
        animatorSet.start();

    }

    public void descerMotoristaAnimation() {
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(lnlDadosMotorista, "y", 2000f);
        animatorX.setDuration(animatioDuration);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animatorX);
        animatorSet.start();

    }

    public void subirInformacoesCorridaAnimation() {
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(llValorAproximado, "y", 500f);
        animatorX.setDuration(animatioDuration);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animatorX);
        animatorSet.start();

    }


    public void descerInformacoesCorridaAnimation() {
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(llValorAproximado, "y", 2000f);
        animatorX.setDuration(animatioDuration);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animatorX);
        animatorSet.start();

    }

    public void voltarBotaoYouGoAnimation() {
        ObjectAnimator animatorX = ObjectAnimator.ofFloat(btnPedirYouGo, "x", 125f);
        animatorX.setDuration(animatioDuration2);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animatorX);
        animatorSet.start();

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Você realmente deseja sair?")
                .setCancelable(false)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                        limparSessao();
                        if (task != null) {
                            task.cancel();
                        }
                        MainActivity.this.finish();
                        LoginManager.getInstance().logOut();
                    }
                })
                .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void limparSessao() {

        sessao.setEmailAjuda("");
        sessao.setTelefoneAjuda("");
        sessao.setIdUsuario("");
        sessao.setNomePassageiro("");
        sessao.setMotivoCancelamentoCorridaRealizada("");
        sessao.setPlacaCarroCorridaRealizada("");
        sessao.setCarroCorridaRealizada("");
        sessao.setKmCorridaRealizada("");
        sessao.setDataFimCorridaRealizada("");
        sessao.setIdUsuarioCorridaRealizada("");
        sessao.setNomeUsuarioCorridaRealizada("");
        sessao.setEnderecoDestinoCorridaRealizada("");
        sessao.setEnderecoOrigemCorridaRealizada("");
        sessao.setValorCorridaRealizada("");
        sessao.setDataCorridaRealizada("");
        sessao.setIDCorridaRealizada("");
        sessao.setLatitudeOrigem("");
        sessao.setLongitudeOrigem("");
        sessao.setLatitudeDestino("");
        sessao.setLongitudeDestino("");
        sessao.setEnderecoOrigem("");
        sessao.setEnderecoDestino("");
        sessao.setDataCartao("");
        sessao.setNumeroCartao("");
        sessao.setNomeCartao("");
        sessao.setIDPagamento("");
        sessao.setEmailMotorista("");
        sessao.setSenhaMotorista("");


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_camera) {

            Intent intent = new Intent(this, CorridasRealizadasActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_gallery) {

            Intent intent = new Intent(this, PagamentosActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_slideshow) {

            Intent intent = new Intent(this, AvaliacoesRecebidasActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_manage) {
            Intent intent = new Intent(this, AjudaActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_exit) {
            onBackPressed();


        } else if (id == R.id.nav_perfil) {
            Intent intent = new Intent(this, EditarPerfilGeralActivity.class);
            startActivity(intent);


        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
//        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setTrafficEnabled(true);
        mMap.setIndoorEnabled(true);
        mMap.setBuildingsEnabled(true);
//        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().isMyLocationButtonEnabled();
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myCurrentLocation, 23));



    }
//
//    private float getBearing(LatLng begin, LatLng end) {
//        double lat = Math.abs(begin.latitude - end.latitude);
//        double lng = Math.abs(begin.longitude - end.longitude);
//
//        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
//            return (float) (Math.toDegrees(Math.atan(lng / lat)));
//        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
//            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
//        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
//            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
//        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
//            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
//        return -1;
//    }
//
//    private List<LatLng> decodePoly(String encoded) {
//        List<LatLng> poly = new ArrayList<LatLng>();
//        int index = 0, len = encoded.length();
//        int lat = 0, lng = 0;
//
//        while (index < len) {
//            int b, shift = 0, result = 0;
//            do {
//                b = encoded.charAt(index++) - 63;
//                result |= (b & 0x1f) << shift;
//                shift += 5;
//            } while (b >= 0x20);
//            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
//            lat += dlat;
//
//            shift = 0;
//            result = 0;
//            do {
//                b = encoded.charAt(index++) - 63;
//                result |= (b & 0x1f) << shift;
//                shift += 5;
//            } while (b >= 0x20);
//            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
//            lng += dlng;
//
//            LatLng p = new LatLng((((double) lat / 1E5)),
//                    (((double) lng / 1E5)));
//            poly.add(p);
//        }
//
//        return poly;
//
//    }

    @Override
    public void onClick(View view) {
        if (view == btnPedirYouGo) {
            botaoYouGoAnimation();
            dadosMotoristaAnimation();
            listarFormasPagamento();
//            solicitar_corrida();
            if (dialog != null) {
                dialog.dismiss();
            }
        } else if (view == btnCancelarCorrida) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Você realmente deseja cancelar a corrida?")
                    .setCancelable(false)
                    .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            cancelarText();


                        }
                    })
                    .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        } else if (view == btnPedirYougo2) {
            solicitar_corrida();
            descerMotoristaAnimation();

        } else if (view == edtQuantidadeMalas) {
            dadosMotoristaAnimation2();
        }

    }

    public void cancelarText() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View termos = getLayoutInflater().inflate(R.layout.modal_digite_uma_msg, null);


        btnConcluirBloquinho = termos.findViewById(R.id.btnConcluirBloquinho);
        bloquinho = termos.findViewById(R.id.edtDigiteUmaMsgBloquinho);


        builder.setView(termos);
        final AlertDialog dialog = builder.create();
        dialog.show();


        btnConcluirBloquinho.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (bloquinho.getText() != null) {
                    motivoCancelamento = bloquinho.getText().toString();
                    cancelar_corrida();

                }


                dialog.dismiss();


            }
        });
    }

    public void avaliarMotoristaTela() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View termos = getLayoutInflater().inflate(R.layout.modal_avaliar_motorista, null);


        btnAvaliarMotorista = termos.findViewById(R.id.btnAvaliarMotorista);
        ratingBarPontualidadeMotorista = termos.findViewById(R.id.ratingBarPontualidadeMotorista);
        ratingBarSimpatiaMotorista = termos.findViewById(R.id.ratingBarSimpatiaMotorista);
        ratingBarCarroMotorista = termos.findViewById(R.id.ratingBarCarroMotorista);
        ratingBarServicoMotorista = termos.findViewById(R.id.ratingBarServicoMotorista);

        ratingBarPontualidadeMotorista.setOnClickListener(this);

        ratingBarPontualidadeMotorista.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rating1 = (int) rating;

            }
        });

        ratingBarSimpatiaMotorista.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rating2 = (int) rating;

            }
        });

        ratingBarCarroMotorista.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rating3 = (int) rating;

            }
        });

        ratingBarServicoMotorista.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rating4 = (int) rating;

            }
        });


        builder.setView(termos);
        final AlertDialog dialog = builder.create();
        dialog.show();


        btnAvaliarMotorista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                avaliar_corrida();

                dialog.dismiss();


            }
        });
    }

    public void solicitar_corrida() {


        Map<String, String> parametros = new HashMap<>();
        parametros.put("km_corrida", distanciaEmKMRota.replace(" km", "").replace(" m", ""));
        parametros.put("qtd_malas", edtQuantidadeMalas.getText().toString());
        parametros.put("qtd_passageiros", edtQuantidadePassageiros.getText().toString());
        parametros.put("latitude_origem", sessao.getLatitudeOrigem());
        parametros.put("longitude_origem", sessao.getLongitudeOrigem());
        parametros.put("endereco_origem", sessao.getEnderecoOrigem());
        parametros.put("latitude_destino", sessao.getlatitudeDestino());
        parametros.put("longitude_destino", sessao.getLongitudeDestino());
        parametros.put("endereco_destino", sessao.getEnderecoDestino());
        parametros.put("fk_forma_pagamento", idCartaoSelecionado);
        parametros.put("cidade_origem", sessao.getCidadeOrigem());


        System.out.println(parametros);

        WebService ws = new WebService(
                getString(R.string.servidor),
                "solicitar_corrida",
                "POST",
                parametros,
                null,
                this,
                true
        );


        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");
                    id_corrida = objeto.getString("id_corrida");


                    sessao.setIdCorrida(id_corrida);


                    System.out.println("STATUS DA CORRIDA É = " + status);
                    System.out.println("RESULTADO DA CORRIDA É = " + resultado);
                    System.out.println("ID DA CORRIDA É = " + id_corrida);


                    if (status == 1) {
                        Toast.makeText(MainActivity.this, "Sua corrida foi pedida com sucesso.", Toast.LENGTH_SHORT).show();
                        btnCancelarCorrida.setVisibility(View.VISIBLE);
                        repetirTask();
                    } else {
                        Toast.makeText(MainActivity.this, "Erro ao buscar a corrida, tente novamente.", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }

    public void cancelar_corrida() {


        Map<String, String> parametros = new HashMap<>();
        parametros.put("id_corrida", sessao.getIdCorrida());
        parametros.put("justificativa", motivoCancelamento);


        System.out.println(parametros);

        WebService ws = new WebService(
                getString(R.string.servidor),
                "cancelar_corrida",
                "POST",
                parametros,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");


                    System.out.println("STATUS DA CORRIDA É = " + status);
                    System.out.println("RESULTADO DA CORRIDA É = " + resultado);


                    if (status == 1) {
                        Toast.makeText(MainActivity.this, "Sua corrida foi cancelada com sucesso.", Toast.LENGTH_SHORT).show();
                        btnCancelarCorrida.setVisibility(View.GONE);
                        task.cancel();
                        voltarBotaoYouGoAnimation();
                    } else {
                        Toast.makeText(MainActivity.this, "Erro ao cancelar a corrida, tente novamente.", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }

    public void custo_corrida() {

        cupomDigitado = edtCupom.getText().toString().trim();

        //
//        if (cupom.equals("")) {
//            cupom = "";
//        }else{
//            cupom = edtCupom.getText().toString().trim();
//
//        }


        WebService ws = new WebService(
                getString(R.string.servidor),
                "custo_corrida?cidade_origem=" + sessao.getCidadeOrigem() + "&cupom=" + cupomDigitado + "&latitude_origem=" + latitudePartida + "&longitude_origem=" + longitudePartida
                        + "&latitude_destino=" + latitudeDestino + "&longitude_destino=" + longitudeDestino,
                "GET",
                null,
                null,
                this,
                false

        );


        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");
                    String valor = objeto.getString("valor");
                    String km = objeto.getString("km");
                    String tempo = objeto.getString("tempo");
//                    JSONObject cupom = objeto.getJSONObject("cupom");


//                    if (cupom.toString().equals("null")) {
//
//                        String id_cupom = cupom.getString("id_cupom");
//                        String nome_cupom = cupom.getString("nome_cupom");
//                        double valor_desconto_cupom = cupom.getDouble("valor_desconto_cupom");
//                        String tipo_desconto_cupom = cupom.getString("tipo_desconto_cupom");
//
//                        System.out.println("ID DO COPUM É = " + id_cupom);
//                        System.out.println("CUSTO DA CORRIDA É = " + nome_cupom);
//                        System.out.println("O VALOR DO DESCONTO É = " + valor_desconto_cupom);
//                        System.out.println("O TIPO DO DESCONTO É = " + tipo_desconto_cupom);
//
//                        txtValorAproximado.setText("R$ " + valor_desconto_cupom);
//
//                    }

                    System.out.println("STATUS DA CORRIDA É = " + status);
                    System.out.println("RESULTADO DA CORRIDA É = " + resultado);
                    System.out.println("VALOR É = " + "R$" + valor);
                    System.out.println("KM É = " + km);
                    System.out.println("TEMPO É = " + tempo);

                    int kmMenos1 = Integer.parseInt(km);


                    if (status == 1) {
                        if (kmMenos1 == -1) {
                            custo_corrida();
                        } else {
                            txtValorAproximado.setText("R$ " + valor);
                            int duracaoCorrida = Integer.parseInt(tempo);

                            System.out.println("DURAÇÃO CORRIDA = " + duracaoCorrida);
                            txtTempoEstimado.setText(tempo + " min(s)");
                            desenharRota();
                        }

                    } else if (status == 0) {
                        custo_corrida();
                        Toast.makeText(MainActivity.this, resultado, Toast.LENGTH_SHORT).show();
                    }

                    distanciaEmKMRota = km;


                } catch (
                        Exception e)

                {
                    e.printStackTrace();
                }

            }
        });
    }

    public void acompanhar_motorista() {


        Map<String, String> parametros = new HashMap<>();
        parametros.put("id_corrida", sessao.getIdCorrida());


        System.out.println(parametros);


        WebService ws = new WebService(
                getString(R.string.servidor),
                "acompanhar_corrida",
                "POST",
                parametros,
                null,
                this,
                false

        );


        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");


                    JSONObject arrayInterno = objeto.getJSONObject("dados");

                    String cod = arrayInterno.getString("cod");
                    String status_corrida = arrayInterno.getString("status");
                    String custo_corrida = arrayInterno.getString("custo_corrida");
                    String distancia = arrayInterno.getString("distancia");
                    String latitude_motorista = arrayInterno.getString("latitude_motorista");
                    String longitude_motorista = arrayInterno.getString("longitude_motorista");
                    String nota_media = arrayInterno.getString("nota_media");


                    System.out.println("STATUS DA CORRIDA É = " + status);
                    System.out.println("RESULTADO DA CORRIDA É = " + resultado);

                    if (!latitude_motorista.equals("null") && !longitude_motorista.equals("null")) {
                        latLngMotorista = new LatLng(Double.parseDouble(latitude_motorista), Double.parseDouble(longitude_motorista));
                    }

                    //TODO TRATAR CODIGO DO STATUS - TRAÇAR DO MOTORISTA ATÉ MINHA LOCALIZAÇÃO

                    /*STATUS 83 - BUSCANDO MOTORISTA - FEITO AUTOMATICAMENTE NA PRIMEIRA CHAMADA DA FUNÇÃO
                    STATUS 84 - AGUARDANDO MOTORISTA -
                    STATUS 85 - CORRIDA COMEÇOU
                    STATUS 86 - CORRIDA FOI FINALIZADA COM SUCESSO
                    STATUS 87 - PASSAGEIRO CANCELOU
                    STATUS 88 - MOTORISTA CANCELOU*/

                    System.out.println("CODIGO DO STATUS DA CORRIDA É = " + cod);

                    if (cod.equals("83")) {
                        Toast.makeText(MainActivity.this, "Buscando motorista... Aguarde!", Toast.LENGTH_SHORT).show();
                    }

                    if (cod.equals("84")) {
                        Toast.makeText(MainActivity.this, "Motorista chegando...", Toast.LENGTH_SHORT).show();
                        //FAZ O PIN ANDAR VIA POST ATÉ MINHA LOCALIZAÇÃO
                        Log.w("tracou? = ", String.valueOf(tracou));
                        if (!tracou) {
                            desenharRotaDoMotoristaAteMinhaLocalizacao();
                        }
                        moverPinAteMinhaLocalizacao();
                    }
                    if (cod.equals("85")) {
                        Toast.makeText(MainActivity.this, "A corrida iniciou!", Toast.LENGTH_SHORT).show();
                        //FAZ O PIN ANDAR DA ORIGEM AO DESTINO
                        btnCancelarCorrida.setVisibility(View.GONE);
                        moverPinDaOrigemAteDestino();
                    }
                    if (cod.equals("86")) {
                        Toast.makeText(MainActivity.this, "A corrida foi finalizada com sucesso!", Toast.LENGTH_SHORT).show();
                        task.cancel();
                        avaliarMotoristaTela();
                        voltarBotaoYouGoAnimation();
                    }
                    if (cod.equals("87")) {
                        Toast.makeText(MainActivity.this, "A corrida foi cancelada pelo passageiro!", Toast.LENGTH_SHORT).show();
                    }
                    if (cod.equals("88")) {
                        Toast.makeText(MainActivity.this, "Corrida cancelada pelo motorista.", Toast.LENGTH_SHORT).show();
                        btnCancelarCorrida.setVisibility(View.GONE);
                        task.cancel();
                        voltarBotaoYouGoAnimation();
                    }


                    System.out.println("STATUS EM TEXTO DA CORRIDA É = " + status_corrida);
                    System.out.println("CUSTO DA CORRIDA É = " + custo_corrida);
                    System.out.println("A DISTANCIA É = " + distancia);
                    System.out.println("A LATITUDE DO MOTORISTA É = " + latitude_motorista);
                    System.out.println("A LONGITUDE DO MOTORISTA É = " + longitude_motorista);
                    System.out.println("A NOTA MEDIA DO MOTORISTA É = " + nota_media);

//                    if (!latitude_motorista.equals("null") && !longitude_motorista.equals("null")) {
//                        latLngMotorista = new LatLng(Double.parseDouble(latitude_motorista), Double.parseDouble(longitude_motorista));
//                    }
//                    latLngPassageiro = new LatLng(latitudePartida, longitudePartida);
//
//                    if (!latitude_motorista.equals("null") && !longitude_motorista.equals("null")) {
//                        latLngMotorista = new LatLng(Double.parseDouble(latitude_motorista), Double.parseDouble(longitude_motorista));
//                    }
//                    System.out.println("LATLNG VEIO? = " + latLng);


//                    if (status_corrida.equals("Cancelado pelo motorista")) {
//
//                    } else if (status_corrida.equals("Em Andamento")) {
//                        btnCancelarCorrida.setVisibility(View.GONE);
//
//                    } else if (status_corrida.equals("Corrida Finalizada")) {
//                        task.cancel();
//                        avaliarMotoristaTela();
//                        voltarBotaoYouGoAnimation();
//
//                    }


                    if (status == 1) {

//                        arrayMarkers.add(latLngMotorista);

                        System.out.println("ARRAY MARKERSSSSS = " + arrayMarkers.toString());


//                        Toast.makeText(MainActivity.this, "Buscando motorista... Aguarde!", Toast.LENGTH_SHORT).show();


//                        Toast.makeText(MainActivity.this, "Sua corrida foi cancelada com sucesso.", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(MainActivity.this, "Nenhum motorista encontrado!", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }

    private void moverPinDaOrigemAteDestino() {

        if (latLngMotorista != null) {
            System.out.println("LAT LNG DO MOTÔ ATÉ MINHA LOCALIZAÇÃO = " + latLngMotorista.toString());
        }

        locationListener2 = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                System.out.println(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };


    }

    private void moverPinAteMinhaLocalizacao() {

        if (latLngMotorista != null) {
            System.out.println("LAT LNG DO MOTÔ  = " + latLngMotorista.toString());

            MarkerOptions markerOptions2 = new MarkerOptions()
                    .position(latLngMotorista)
                    .icon(BitmapDescriptorFactory.defaultMarker());
            mMap.addMarker(markerOptions2);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngMotorista, 18));
            task.cancel();
            acompanhar_motorista();

        }
    }

    private void desenharRota() {

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

//        System.out.println("Lat e Lng Origem = "+latitudePartida+longitudePartida+ ": Lat e Lgn Destino = "+latitudeDestino+longitudeDestino);

        if (arrayMarkers.size() > 1) {
            arrayMarkers.clear();
            mMap.clear();

//            if (focusViews != null) {
//                rlCirculoPreto.removeView(focusViews);
//            }
        }

//        arrayMarkers.add(latLng);

        latLngP = new LatLng(latLngPartida.latitude, latLngPartida.longitude);

        latLngD = new LatLng(latLngDestino.latitude, latLngDestino.longitude);
        markerOptions = new MarkerOptions();
        markerOptions2 = new MarkerOptions();
        markerOptions.position(latLngP);
        markerOptions2.position(latLngD);

        builder.include(markerOptions.getPosition());
        builder.include(markerOptions2.getPosition());


        arrayMarkers.add(latLngP);
        arrayMarkers.add(latLngD);


        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.30); // offset from edges of the map 30% of screen

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        mMap.animateCamera(cu);

        if (arrayMarkers.size() == 1) {
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        } else if (arrayMarkers.size() == 2) {
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        }

        mMap.addMarker(markerOptions);
        mMap.addMarker(markerOptions2);

        if (arrayMarkers.size() >= 2) {

//            rlCirculoPreto.setVisibility(View.VISIBLE);
//            rlCirculoPreto.addView(focusViews);

//            origin = (LatLng) arrayMarkers.get(0);
//            dest = (LatLng) arrayMarkers.get(1);

            origin = (LatLng) arrayMarkers.get(0);
            dest = (LatLng) arrayMarkers.get(1);

            url = getDirectionsUrl(origin, dest);
            downloadTask = new DownloadTask();
            downloadTask.execute(url);


            System.out.println("URL " + url);

        }
    }

    private void desenharRotaDoMotoristaAteMinhaLocalizacao() {

        LatLngBounds.Builder builder = new LatLngBounds.Builder();

//        System.out.println("Lat e Lng Origem = "+latitudePartida+longitudePartida+ ": Lat e Lgn Destino = "+latitudeDestino+longitudeDestino);

        if (arrayMarkers.size() > 1) {
            arrayMarkers.clear();
            mMap.clear();

//            if (focusViews != null) {
//                rlCirculoPreto.removeView(focusViews);
//            }
        }

//        arrayMarkers.add(latLng);

        latLngP = new LatLng(latLngPartida.latitude, latLngPartida.longitude);
        latLngD = new LatLng(latLngMotorista.latitude, latLngMotorista.longitude);
        markerOptions = new MarkerOptions();
        markerOptions2 = new MarkerOptions();
        markerOptions.position(latLngP);
        markerOptions2.position(latLngD);

        builder.include(markerOptions.getPosition());
        builder.include(markerOptions2.getPosition());


        arrayMarkers.add(latLngP);
        arrayMarkers.add(latLngD);


        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.30); // offset from edges of the map 30% of screen

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        mMap.animateCamera(cu);

        if (arrayMarkers.size() == 1) {
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        } else if (arrayMarkers.size() == 2) {
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        }

        mMap.addMarker(markerOptions);
        mMap.addMarker(markerOptions2);

        if (arrayMarkers.size() >= 2) {

//            rlCirculoPreto.setVisibility(View.VISIBLE);
//            rlCirculoPreto.addView(focusViews);

//            origin = (LatLng) arrayMarkers.get(0);
//            dest = (LatLng) arrayMarkers.get(1);

            origin = (LatLng) arrayMarkers.get(0);
            dest = (LatLng) arrayMarkers.get(1);

            url = getDirectionsUrl(origin, latLngMotorista);
            downloadTask = new DownloadTask();
            downloadTask.execute(url);
            tracou = true;


            System.out.println("URL " + url);

        }

    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        //Origem
        String origem = "origin=" + origin.latitude + "," + origin.longitude;

        //Destino
        String destino = "destination=" + dest.latitude + "," + dest.longitude;

        //Sensor carro do motorista
        String sensor = "sensor=false";
        String modo = "mode=driving";

        //Parâmetros
        String parametros = origem + "&" + destino + "&" + sensor + "&" + modo;

        //Output
        String output = "json";

        //URL
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parametros;

        return url;

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private String downloadUrl(String strUrl) throws IOException {

        String data = "";
        HttpURLConnection urlConnection = null;
        URL url = new URL(strUrl);

        urlConnection = (HttpURLConnection) url.openConnection();

        urlConnection.connect();

        try (InputStream iStream = urlConnection.getInputStream()) {
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuilder sb = new StringBuilder();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            urlConnection.disconnect();
        }
        return data;


    }

    @SuppressLint("StaticFieldLeak")
    private class DownloadTask extends AsyncTask<String, Void, String> {

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();


            parserTask.execute(result);

        }


        private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

            // Parsing the data in non-ui thread
            @Override
            protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

                JSONObject jObject;
                List<List<HashMap<String, String>>> routes = null;

                try {
                    jObject = new JSONObject(jsonData[0]);
                    DirectionsJSONParser parser = new DirectionsJSONParser();

                    routes = parser.parse(jObject);

//                    System.out.println("ROTA = "+jObject.toString(5));
//                    System.out.println("ROTA = " + jObject.getJSONArray("routes").toString(4));

                    for (int i = 0; i < routes.size(); i++) {
                        JSONArray rota = jObject.getJSONArray("routes");

                        JSONObject arrayInterno = rota.getJSONObject(i);

                        JSONArray legs = arrayInterno.getJSONArray("legs");

                        for (int j = 0; j < legs.length(); j++) {

                            JSONObject arrayInterno2 = legs.getJSONObject(i);

                            JSONObject distance = arrayInterno2.getJSONObject("distance");

                            String distancia_km = distance.getString("text");

                            System.out.println("DISTÂNCIA EM KM = " + distancia_km);
                            distanciaEmKMRota = distancia_km;

                            JSONObject duration = arrayInterno2.getJSONObject("duration");

                            String duracao_viagem = duration.getString("text");

                            System.out.println("TEMPO ESTIMADO = " + duracao_viagem);

                        }


                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                return routes;
            }

            @Override
            protected void onPostExecute(List<List<HashMap<String, String>>> result) {
                ArrayList points = new ArrayList();

                PolylineOptions lineOptions = null;
                markerOptions = new MarkerOptions();

                for (int i = 0; i < result.size(); i++) {
                    lineOptions = new PolylineOptions();

                    List<HashMap<String, String>> path = result.get(i);

                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    lineOptions.addAll(points);
                    lineOptions.width(12);
                    lineOptions.color(Color.BLACK);
                    lineOptions.geodesic(true);

                }

                if (lineOptions != null) {
                    mMap.addPolyline(lineOptions);
                }
            }
        }


    }

    public void repetirTask() {
        final Handler handler = new Handler();
        Timer timer = new Timer();

        task = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            System.out.println("CHEGOU NO TIMER");
                            acompanhar_motorista();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        timer.schedule(task, 0, 5000);

    }

    public void avaliar_corrida() {


        Map<String, String> parametros = new HashMap<>();
        parametros.put("id_corrida", sessao.getIdCorrida());
        parametros.put("pontualidade_motorista", String.valueOf(rating1));
        parametros.put("simpatia_motorista", String.valueOf(rating2));
        parametros.put("carro_motorista", String.valueOf(rating3));
        parametros.put("servico_motorista", String.valueOf(rating4));


        System.out.println(parametros);

        WebService ws = new WebService(
                getString(R.string.servidor),
                "avaliar_corrida",
                "POST",
                parametros,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");


                    System.out.println("STATUS DO AVALIAR MOTORISTA É = " + status);
                    System.out.println("RESULTADO DO AVALIAR MOTORISTA É = " + resultado);


                    if (status == 1) {
                        Toast.makeText(MainActivity.this, "Avaliação enviada com secesso.", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(MainActivity.this, "Erro ao efetuar a avaliação, tente novamente.", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }

    public void listarFormasPagamento() {

        WebService ws = new WebService(
                getString(R.string.servidor),
                "listar_formas_pagamento",
                "GET",
                null,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");
                    JSONArray formas = objeto.getJSONArray("formas");


                    System.out.println("STATUS DA LISTAGEM DAS FORMAS DE PAGAMENTO É = " + status);
                    System.out.println("RESULTADO DA LISTAGEM DAS FORMAS DE PAGAMENTO É = " + resultado);


//                    Adapter_Formas_Pagamento adapter_formas_pagamento = new Adapter_Formas_Pagamento(getApplicationContext(), R.layout.list_item_formas_pagamento, arrayListFomaPagamento);
//
//                    mylistview.setAdapter(adapter_formas_pagamento);

                    for (int x = 0; x < formas.length(); x++) {
                        JSONObject arrayInterno = formas.getJSONObject(x);

//                        Controller_Formas_Pagamento hashMap = new Controller_Formas_Pagamento();

                        String id_pagamento = arrayInterno.getString("id_pagamento");
                        String nome_cartao = arrayInterno.getString("nome_cartao");
                        String numero_cartao = arrayInterno.getString("numero_cartao");
                        String data_vencimento_cartao = arrayInterno.getString("data_vencimento_cartao");


                        System.out.println("O ID DO PAGAMENTO É = " + id_pagamento);
                        System.out.println("O NOME DO CARTÃO É = " + nome_cartao);
                        System.out.println("O NÚMERO DO CARTÃO É = " + numero_cartao);
                        System.out.println("A DATA DE VENTCIMENTO DO CARTÃO É = " + data_vencimento_cartao);
                        arrayNumeroCartao.clear();

                        arrayNumeroCartao.add(numero_cartao);
                        arrayIdCartao.add(id_pagamento);


                        arrayAdapterCartao = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, arrayNumeroCartao);
                        spnEscolhaPagamento.setAdapter(arrayAdapterCartao);

                        spnEscolhaPagamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                cartaoSelecionado = arrayNumeroCartao.get(position);
                                idCartaoSelecionado = arrayIdCartao.get(position);

                                System.out.println("NOME DO CARTÃO = " + cartaoSelecionado);
                                System.out.println("ID DO CARTÃO = " + idCartaoSelecionado);

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


//                        hashMap.put(Controller_Formas_Pagamento.id_pagamento, id_pagamento);
//                        hashMap.put(Controller_Formas_Pagamento.nome_cartao, nome_cartao);
//                        hashMap.put(Controller_Formas_Pagamento.numero_cartao, numero_cartao);
//                        hashMap.put(Controller_Formas_Pagamento.data_vencimento_cartao, data_vencimento_cartao);


//                        arrayListFomaPagamento.add(hashMap);
//
//                        arrayIDFormaPagamento.add(id_pagamento);
//                        arrayNomeCartao.add(nome_cartao);
//                        arrayNumeroCartao.add(numero_cartao);
//                        arrayDataCartao.add(data_vencimento_cartao);
//
//                        System.out.println("O ARRAY DA FORMA DE PAGAMENTO É = " + arrayListFomaPagamento.toString());


//                        mylistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                            @Override
//                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                                System.out.println("O INDEX DO PAGAMENTO É = " + position);
//
//
//                                sessao.setIDPagamento(arrayIDFormaPagamento.get(position));
//                                sessao.setNomeCartao(arrayNomeCartao.get(position));
//                                sessao.setNumeroCartao(arrayNumeroCartao.get(position));
//                                sessao.setDataCartao(arrayDataCartao.get(position));
//
//
//                                System.out.println("O ID DO PAGAMENTO É = " + arrayIDFormaPagamento.get(position));
//                                finish();
//
//                                Intent intent = new Intent(PagamentosActivity.this, EditarPagamentoActivity.class);
//                                startActivity(intent);
//                            }
//                        });


                    }

                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }

    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            Log.w("Minha localização: ", location.toString());

//            myChangedLocation = new LatLng(location.getLatitude(), location.getLongitude());
//
//            if (arrayMarkers.size() >= 1) {
//                arrayMarkers.clear();
//            }else {
//                MarkerOptions markerOptions = new MarkerOptions().position(myChangedLocation);
//                mMap.addMarker(markerOptions);
//                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myChangedLocation, 20));
//
////            listPosicoes.add(myChangedLocation);
//                arrayMarkers.add(listPosicoes);
//
//                System.out.println("MARCADORES ARRAY = " + arrayMarkers.size());
////            System.out.println("MARCADORES LIST = " + listPosicoes.size());
//            }

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            System.out.println(provider + ": " + status);

        }

        @Override
        public void onProviderEnabled(String provider) {
            System.out.println(provider + "= Ligado");

        }

        @Override
        public void onProviderDisabled(String provider) {
            System.out.println(provider + "= Desligado");

        }


    }


}
