package com.brasil.naville.yougoclienteandroid;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.brasil.naville.yougoclienteandroid.Adapter.Adapter_Formas_Pagamento;
import com.brasil.naville.yougoclienteandroid.Controller.Controller_Formas_Pagamento;
import com.brasil.naville.yougoclienteandroid.WS.WebService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PagamentosActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgVoltarPagamentos;
    TextView txtCadastraroCartao;
    private Context context;

    ArrayList<Controller_Formas_Pagamento> arrayListFomaPagamento = new ArrayList<>();
    final List<String> arrayIDFormaPagamento = new ArrayList<>();
    final List<String> arrayNomeCartao = new ArrayList<>();
    final List<String> arrayNumeroCartao = new ArrayList<>();
    final List<String> arrayDataCartao = new ArrayList<>();
    List<RowItem2> rowItems2;
    ListView mylistview;

    AppShared sessao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagamentos);

        imgVoltarPagamentos = findViewById(R.id.imgVoltarPagamentos);
        txtCadastraroCartao = findViewById(R.id.txtCadastraroCartao);

        sessao = new AppShared(this);


        imgVoltarPagamentos.setOnClickListener(this);
        txtCadastraroCartao.setOnClickListener(this);

        listarFormasPagamento();


        mylistview = findViewById(R.id.lstFormasDePagamento);


    }


    @Override
    public void onClick(View v) {

        if (v == imgVoltarPagamentos) {
            finish();
        } else if (v == txtCadastraroCartao) {
            finish();
            Intent intent = new Intent(this, CadastrarPagamentosActivity.class);
            startActivity(intent);

        }

    }


    public void listarFormasPagamento() {


        Map<String, String> parametros = new HashMap<>();


        WebService ws = new WebService(
                getString(R.string.servidor),
                "listar_formas_pagamento",
                "GET",
                null,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores dos modelos são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");
                    String formas_pagamento = objeto.getString("formas");
                    JSONArray formas = objeto.getJSONArray("formas");


                    if (formas.length() == 0) {
                        Toast.makeText(PagamentosActivity.this, "Você ainda não tem cartão cadastrado, cadastre agora.", Toast.LENGTH_SHORT).show();
                    }


                    System.out.println("STATUS DA LISTAGEM DAS FORMAS DE PAGAMENTO É = " + status);
                    System.out.println("RESULTADO DA LISTAGEM DAS FORMAS DE PAGAMENTO É = " + resultado);
                    System.out.println("AS FORMAS DE PAGAMENTOS SÃO: " + formas_pagamento);
                    System.out.println("AS FORMAS FORMAS SÃO: " + formas);


                    Adapter_Formas_Pagamento adapter_formas_pagamento = new Adapter_Formas_Pagamento(getApplicationContext(), R.layout.list_item_formas_pagamento, arrayListFomaPagamento);

                    mylistview.setAdapter(adapter_formas_pagamento);

                    for (int x = 0; x < formas.length(); x++) {
                        JSONObject arrayInterno = formas.getJSONObject(x);

                        Controller_Formas_Pagamento hashMap = new Controller_Formas_Pagamento();

                        String id_pagamento = arrayInterno.getString("id_pagamento");
                        String nome_cartao = arrayInterno.getString("nome_cartao");
                        String numero_cartao = arrayInterno.getString("numero_cartao");
                        String data_vencimento_cartao = arrayInterno.getString("data_vencimento_cartao");


                        hashMap.put(Controller_Formas_Pagamento.id_pagamento, id_pagamento);
                        hashMap.put(Controller_Formas_Pagamento.nome_cartao, nome_cartao);
                        hashMap.put(Controller_Formas_Pagamento.numero_cartao, numero_cartao);
                        hashMap.put(Controller_Formas_Pagamento.data_vencimento_cartao, data_vencimento_cartao);


                        arrayListFomaPagamento.add(hashMap);

                        arrayIDFormaPagamento.add(id_pagamento);
                        arrayNomeCartao.add(nome_cartao);
                        arrayNumeroCartao.add(numero_cartao);
                        arrayDataCartao.add(data_vencimento_cartao);

                        System.out.println("O ARRAY DA FORMA DE PAGAMENTO É = " + arrayListFomaPagamento.toString());


                        mylistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                System.out.println("O INDEX DO PAGAMENTO É = " + position);


                                sessao.setIDPagamento(arrayIDFormaPagamento.get(position));
                                sessao.setNomeCartao(arrayNomeCartao.get(position));
                                sessao.setNumeroCartao(arrayNumeroCartao.get(position));
                                sessao.setDataCartao(arrayDataCartao.get(position));


                                System.out.println("O ID DO PAGAMENTO É = " + arrayIDFormaPagamento.get(position));
                                finish();

                                Intent intent = new Intent(PagamentosActivity.this, EditarPagamentoActivity.class);
                                startActivity(intent);
                            }
                        });


                    }

                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }


}
