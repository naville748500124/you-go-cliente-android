package com.brasil.naville.yougoclienteandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class PoliticaActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgVoltarPolitica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_politica);

        imgVoltarPolitica = findViewById(R.id.imgVoltarPolitica);


        imgVoltarPolitica.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view == imgVoltarPolitica) {
            finish();
        }

    }
}
