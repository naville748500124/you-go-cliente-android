package com.brasil.naville.yougoclienteandroid;

import android.annotation.SuppressLint;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.brasil.naville.yougoclienteandroid.WS.WebService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class RestaurarSenhaActivity extends AppCompatActivity implements View.OnClickListener {


    ImageView imgVoltarRestaurarSenha;
    LinearLayout lnlRecuperar;
    Button btnRecuperar;
    EditText edtEmailRecuperar;
    String email;
    String emailPattern;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurar_senha);


        imgVoltarRestaurarSenha = findViewById(R.id.imgVoltarRestaurarSenha);
        lnlRecuperar = findViewById(R.id.lnlRecuperar);
        btnRecuperar = findViewById(R.id.btnRecuperar);
        edtEmailRecuperar = findViewById(R.id.edtEmailRecuperar);


        imgVoltarRestaurarSenha.setOnClickListener(this);
        lnlRecuperar.setOnClickListener(this);
        btnRecuperar.setOnClickListener(this);


        //Tirar o teclado quando clicar fora
        lnlRecuperar.setOnTouchListener(new View.OnTouchListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                assert imm != null;
                imm.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
                return false;
            }
        });


    }

    @Override
    public void onClick(View view) {

        if (view == imgVoltarRestaurarSenha) {
            finish();
        } else if (view == btnRecuperar) {

            email = edtEmailRecuperar.getText().toString().trim();
            emailPattern = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
            if (email.matches(emailPattern)) {
                chamarWebServiceRestaurar();

            } else {
                Toast.makeText(getApplicationContext(), "Digite um e-mail válido.", Toast.LENGTH_SHORT).show();
            }
        }

    }


    public void chamarWebServiceRestaurar() {

        Map<String, String> parametros = new HashMap<>();
        parametros.put("email_usuario", edtEmailRecuperar.getText().toString());

        WebService ws = new WebService(
                getString(R.string.servidor),
                "esqueci_senha?email_usuario=" + edtEmailRecuperar.getText().toString(),
                "GET",
                null,
                null,
                this,
                true
        );

        ws.getData(new WebService.RetornoAssincrono() {
            @Override
            public void onSuccess(JSONObject objeto) {
                System.out.println("Sucesso!!! Os valores são: \n" + objeto);

                try {
                    int status = objeto.getInt("status");
                    String resultado = objeto.getString("resultado");


                    System.out.println("Status = " + status);
                    System.out.println("Resultado = " + resultado);


                    if (status == 1) {
                        finish();
                        Toast.makeText(getApplicationContext(), "Uma nova senha foi enviada para o seu e-mail.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(RestaurarSenhaActivity.this, resultado, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    System.out.println("Catch : " + e.getMessage());
                }
            }
        });


    }
}
