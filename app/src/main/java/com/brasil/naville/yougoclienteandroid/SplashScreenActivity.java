package com.brasil.naville.yougoclienteandroid;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);



        //Contador para mudar do Launcher para tela inicial
        CountDownTimer timer = new CountDownTimer(3000, 3000) {
            public void onTick(long millisUntilFinished) {
                System.out.println("AGORA VAI");
            }

            public void onFinish() {
                Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        };
        timer.start();


    }
}
