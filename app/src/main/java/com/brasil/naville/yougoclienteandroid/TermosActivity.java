package com.brasil.naville.yougoclienteandroid;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class TermosActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView imgVoltarTermos;
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termos);

        imgVoltarTermos = findViewById(R.id.imgVoltarTermos);
        webView = findViewById(R.id.webViewTermos);
        loadWebViewLoad(webView);


        imgVoltarTermos.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == imgVoltarTermos) {
            finish();
        }

    }


    private void loadWebViewLoad(WebView webview) {
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setSupportMultipleWindows(true);
        webview.setWebViewClient(new WebViewClient());
        webview.setWebChromeClient(new WebChromeClient());
        webview.loadUrl("http://devnaville-br2.16mb.com/yougo/controller_termo/termos_de_uso");
    }
}